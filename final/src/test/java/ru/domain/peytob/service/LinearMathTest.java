package ru.domain.peytob.service;

import org.junit.jupiter.api.Test;
import ru.domain.peytob.model.Point2;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тестирование методов линейной алгебры.
 */
class LinearMathTest {

    /**
     * Тестирование того, как работает определение длины вектора.
     */
    @Test
    void distance() {
        Point2 p1 = new Point2(0, 0);
        Point2 p2 = new Point2(30, 30);
        Point2 p3 = new Point2(60, 102);

        assertEquals(78.0f, LinearMathService.distance(p2, p3));
        assertEquals(42.4264068f, LinearMathService.distance(p1, p2));
    }
}
