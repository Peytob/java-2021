package ru.domain.peytob.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.domain.peytob.databaseProvider.DerbyProvider;
import ru.domain.peytob.model.*;
import ru.domain.peytob.model.exportXml.ResultStruct;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Тестирование сервиса сереализации данных на 1 примере.
 */
class MarshallServiceTest {
    DerbyProvider provider;
    VehicleService vehicleService;

    /**
     * Конвертирует LocalDateTime в Date
     */
    private Date localDateTimeToDate(LocalDateTime dt) {
        return Date.from(dt.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Установка объектов БД для тестирования.
     */
    @BeforeEach
    void beforeEach() throws IOException, SQLException {
        provider = new DerbyProvider();
        vehicleService = new VehicleService(provider.getDataSource());
    }

    @Test
    void marshall() throws SQLException, JAXBException {
        Registration registration = new Registration(0, "12345678");
        registration.setId(vehicleService.insertRegistration(registration));

        VehicleType vehicleType = new VehicleType(1, "Автобус");
        vehicleType.setId(vehicleService.insertType(vehicleType));

        Vehicle vh = new Vehicle(vehicleType,
                registration,
                new Point2(42.3f, 3.14f),
                localDateTimeToDate(LocalDateTime.of(2021, 4, 3, 10, 42, 0)));

        vehicleService.insertVehicle(vh);

        SearchResult result = vehicleService.selectNearby(new Point2(100, 100),
                localDateTimeToDate(LocalDateTime.of(2021, 4, 3, 10, 42, 30)),
                60);

        // нормально проверить, сравнив все варианты выдачи xml не выйдет - в данном случае есть просто
        // огромное количество перестановок. 6! внутри времени и даты, 3! внутри транспортного средства
        // и все это дело еще может между собой меняться местами, давая еще 3!. Надеюсь, рандом выдаст
        // такой же.
        // но с такой строкой, по крайней мере у меня, проходит.

        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<result>\n" +
                "    <datetime>\n" +
                "        <year>2021</year>\n" +
                "        <month>3</month>\n" +
                "        <day>3</day>\n" +
                "        <hour>10</hour>\n" +
                "        <minute>42</minute>\n" +
                "        <second>30</second>\n" +
                "    </datetime>\n" +
                "    <radius>2000.0</radius>\n" +
                "    <vehicles>\n" +
                "        <vehicle>\n" +
                "            <position x=\"42.3\" y=\"3.14\"/>\n" +
                "            <registration>12345678</registration>\n" +
                "            <title>Автобус</title>\n" +
                "        </vehicle>\n" +
                "    </vehicles>\n" +
                "</result>\n";

        assertEquals(expected, new ResultsMarshallService().marshall(new ResultStruct(result)));
    }

    /**
     * Очистка БД. Необходима для уборки мусора перед следующими тестами.
     */
    @AfterEach
    void afterEach() throws SQLException {
        vehicleService.dropTables();
    }
}