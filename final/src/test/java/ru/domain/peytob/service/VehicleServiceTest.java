package ru.domain.peytob.service;

import org.junit.jupiter.api.*;
import ru.domain.peytob.databaseProvider.DerbyProvider;
import ru.domain.peytob.model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Тестирует корректность работы репозитория.
 */
class VehicleServiceTest {
    DerbyProvider provider;
    VehicleService vehicleService;

    /**
     * Сетап БД для проверки. Считаем, что она пустая и неинициализированная.
     */
    @BeforeEach
    void beforeEach() throws IOException, SQLException {
        provider = new DerbyProvider();
        vehicleService = new VehicleService(provider.getDataSource());
    }

    /**
     * Тестирование корректности добавления нового транспорта, номеров и типов.
     */
    @Test
    void connection() throws SQLException {
        Registration registration = new Registration(-1, "C065MK78");
        registration.setId(vehicleService.insertRegistration(registration));
        assertNotEquals(-1, registration.getId());

        VehicleType type = new VehicleType(-1, "Трамвай");
        type.setId(vehicleService.insertType(type));
        assertNotEquals(-1, type.getId());

        Vehicle vehicle = new Vehicle(-1, type, registration, new Point2(100, 200),
                Calendar.getInstance().getTime());
        vehicle.setId(vehicleService.insertVehicle(vehicle));
        assertNotEquals(-1, vehicle.getId());
    }

    @Test
    void select() throws SQLException {
        VehicleType type = new VehicleType(-1, "Автобус");
        type.setId(vehicleService.insertType(type));
        assertNotEquals(-1, type.getId());

        // Установка автомобилей в БД.
        // Каждый автомобиль удаляется от другого на ~ 360.5 метра.
        // Последний будет на 1500, 1000, что в ~ 1803 метрах от центра.

        Vehicle[] vehicles = new Vehicle[5];

        for (int i = 0; i < vehicles.length; i++) {
            final Registration registration = new Registration(-1, "A" + i + "BC");
            registration.setId(vehicleService.insertRegistration(registration));
            assertNotEquals(-1, registration.getId());

            vehicles[i] = new Vehicle(-1, type, registration, new Point2(300 * i, 200 * i),
                    Calendar.getInstance().getTime());

            vehicles[i].setId(vehicleService.insertVehicle(vehicles[i]));
            assertNotEquals(-1, vehicles[i].getId());
        }

        // Выборка всех автомобилей - точка поиска в 0 0 на текущий момент времени.

        SearchResult resultAll = vehicleService.selectNearby(new Point2(0, 0), Calendar.getInstance().getTime(),
                60);

        Map<String, Boolean> numbersAll = new TreeMap<>();
        for (Vehicle item : vehicles) {
            numbersAll.put(item.getRegistration().getNumber(), false);
        }

        for (Vehicle vehicle : resultAll.getResult()) {
            assertTrue(numbersAll.containsKey(vehicle.getRegistration().getNumber()));
            numbersAll.put(vehicle.getRegistration().getNumber(), true);
        }

        numbersAll.values().forEach(Assertions::assertTrue);

        // Выборка автомобилей, кроме первого - точка поиска далеко. Поиск на текущий момент времени.

        SearchResult result2000 = vehicleService.selectNearby(new Point2(1600, 1700), Calendar.getInstance().getTime(),
                60);

        Map<String, Boolean> numbers2000 = new TreeMap<>();
        for (int i = 1; i < vehicles.length; i++) {
            numbers2000.put(vehicles[i].getRegistration().getNumber(), false);
        }

        for (Vehicle vehicle : result2000.getResult()) {
            assertTrue(numbers2000.containsKey(vehicle.getRegistration().getNumber()));
            numbers2000.put(vehicle.getRegistration().getNumber(), true);
        }

        numbers2000.values().forEach(Assertions::assertTrue);
    }

    /**
     * Очистка БД для будущих тестов.
     */
    @AfterEach
    void afterEach() throws SQLException {
        vehicleService.dropTables();
    }
}