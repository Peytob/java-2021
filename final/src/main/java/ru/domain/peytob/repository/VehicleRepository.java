package ru.domain.peytob.repository;

import org.apache.derby.jdbc.EmbeddedDataSource;
import ru.domain.peytob.model.*;
import ru.domain.peytob.service.LinearMathService;

import java.sql.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Репозиторий для транспорта. Реализует небольшой набор select на все случаи жизни.
 */
public class VehicleRepository {
    /**
     * Фактически, пул подключений.
     */
    private final EmbeddedDataSource dataSource;

    /**
     * Название таблицы с транспортом.
     */
    private final String VEHICLE = "Vehicles";

    /**
     * Название таблицы с регистрацией.
     */
    private final String REGISTRATION = "Registration";

    /**
     * Название таблицы с типами транспорта.
     */
    private final String TYPE = "Type";

    public VehicleRepository(EmbeddedDataSource dataSource) throws SQLException {
        this.dataSource = dataSource;

        initializeTables();
    }

    /**
     * Проверка того, что таблица существует.
     *
     * @param tableName Название таблицы.
     * @return True, если такая таблица уже есть.
     * @throws SQLException Если была ошибка во время SQL операций.
     */
    private boolean isTableExists(String tableName) throws SQLException {
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData databaseMetadata = connection.getMetaData();

            final String[] types = new String[]{"TABLE"};

            try (ResultSet resultSet = databaseMetadata.getTables(
                    null, null, tableName.toUpperCase(), types)) {

                return resultSet.next();
            }
        }
    }

    /**
     * Инициализация таблицы студентов.
     *
     * @throws SQLException Если была ошибка во время SQL операций
     */
    private void initializeTables() throws SQLException {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            if (!isTableExists(TYPE)) {
                String sql = "CREATE TABLE " + TYPE + "(" +
                        "id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                        "title VARCHAR(255)" +
                        ")";

                statement.executeUpdate(sql);
            }

            if (!isTableExists(REGISTRATION)) {
                String sql = "CREATE TABLE " + REGISTRATION + "(" +
                        "id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                        "number VARCHAR(8)" +
                        ")";

                statement.executeUpdate(sql);
            }

            if (!isTableExists(VEHICLE)) {
                String sql = "CREATE TABLE " + VEHICLE + "(" +
                        "id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                        "x float, " +
                        "y float, " +
                        "registration INTEGER REFERENCES " + REGISTRATION + "(id)," +
                        "type INTEGER REFERENCES " + TYPE + "(id)," +
                        "time TIMESTAMP " +
                        ")";

                statement.executeUpdate(sql);
            }
        }
    }

    /**
     * Выбирает транспорт в радиусе 2 км. от точки Point в момент времени Time +- SELECT_EPS секунд.
     *
     * @param point Точка, вокруг которой происходит выборка.
     * @param time  Момент времени.
     * @param eps   Допустимая погрешность времени при поиске транспорта (секунды).
     * @return Множество найденных средств транспорта.
     * @throws SQLException Если что-то пошло не так.
     */
    public SearchResult selectNearby(Point2 point, Date time, int eps) throws SQLException {
        String sql = "SELECT * FROM " + VEHICLE + " " +
                "JOIN " + TYPE + " ON " + VEHICLE + ".type = " + TYPE + ".id " +
                "JOIN " + REGISTRATION + " ON " + VEHICLE + ".registration = " + REGISTRATION + ".id " +
                "WHERE (? < time AND time < ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {

            Calendar cal = Calendar.getInstance();

            cal.setTimeInMillis(time.getTime());
            cal.add(Calendar.SECOND, -eps);
            statement.setTimestamp(1, new Timestamp(cal.getTime().getTime()));

            cal.setTimeInMillis(time.getTime());
            cal.add(Calendar.SECOND, eps);
            statement.setTimestamp(2, new Timestamp(cal.getTime().getTime()));

            // Радиус поиска. Согласно заданию устанавливается в 2 км.
            final float radius = 2000.0f;

            try (ResultSet resultSet = statement.executeQuery()) {

                SearchResult result = new SearchResult(radius, time);

                while (resultSet.next()) {
                    float x = resultSet.getFloat("x");
                    float y = resultSet.getFloat("y");
                    Point2 vehiclePoint = new Point2(x, y);

                    if (LinearMathService.distance(vehiclePoint, point) < radius) {
                        VehicleType type = new VehicleType(resultSet.getInt("type"),
                                resultSet.getString("title"));

                        Registration registration = new Registration(resultSet.getInt("registration"),
                                resultSet.getString("number"));

                        Vehicle vehicle = new Vehicle(
                                type,
                                registration,
                                vehiclePoint,
                                new Date(resultSet.getTime("time").getTime()));
                        result.addVehicle(vehicle);
                    }
                }
                return result;
            }
        }
    }

    /**
     * Получает ID добавленных записей, если ID были сгенерированы через автоинкремент.
     *
     * @param statement Statement, после выполнения которого требуется получить ID вставленных записей.
     * @return ID добавленной записи.
     * @throws SQLException Если что-то пошло не так с БД.
     */
    private int getGeneratedKeys(Statement statement) throws SQLException {
        try (ResultSet resultSet = statement.getGeneratedKeys()) {

            int nextId = -1;

            if (resultSet.next()) {
                nextId = resultSet.getInt("1");
            }

            if (nextId == -1) {
                throw new RuntimeException("New id is not found.");
            }

            return nextId;
        }
    }

    /**
     * Добавляет регистрацию в БД.
     * По большому счету нужна для прохождения тестов программой.
     *
     * @param registration Регистрационный номер.
     * @return id номера.
     */
    public int insertRegistration(Registration registration) throws SQLException, RuntimeException {
        String sql = "INSERT INTO " + REGISTRATION + "(number)" +
                "values(?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, registration.getNumber());
            statement.execute();
            return getGeneratedKeys(statement);
        }
    }

    /**
     * Добавляет регистрацию в БД.
     * По большому счету нужна для прохождения тестов программой.
     *
     * @param type Тип транспорта.
     * @return id нового типа.
     */
    public int insertType(VehicleType type) throws SQLException, RuntimeException {
        String sql =
                "INSERT INTO " + TYPE + "(title)" +
                        "values(?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, type.getTitle());
            statement.execute();
            return getGeneratedKeys(statement);
        }
    }

    /**
     * Добавление новой записи о транспорте.
     *
     * @param vehicle Транспорт.
     * @return id вставленного транспорта.
     * @throws SQLException Если что-то пошло не так со стороны БД.
     */
    public int insertVehicle(Vehicle vehicle) throws SQLException, RuntimeException {
        String sql =
                "INSERT INTO " + VEHICLE + "(x, y, registration, type, time)" +
                        "values(?, ?, ?, ?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {

            statement.setFloat(1, vehicle.getPosition().getX());
            statement.setFloat(2, vehicle.getPosition().getY());
            statement.setInt(3, vehicle.getRegistration().getId());
            statement.setInt(4, vehicle.getType().getId());
            Timestamp time = new Timestamp(vehicle.getTime().getTime());
            statement.setTimestamp(5, time);
            statement.execute();

            return getGeneratedKeys(statement);
        }
    }

    /**
     * Удаляет таблицы.
     *
     * @throws SQLException Если что-то не так со стороны БД.
     */
    public void dropTables() throws SQLException {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            statement.execute("DROP TABLE " + VEHICLE);
            statement.execute("DROP TABLE " + REGISTRATION);
            statement.execute("DROP TABLE " + TYPE);
        }
    }
}
