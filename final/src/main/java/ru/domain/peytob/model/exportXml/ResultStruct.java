package ru.domain.peytob.model.exportXml;

import ru.domain.peytob.model.SearchResult;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Calendar;
import java.util.Collection;
import java.util.TimeZone;
import java.util.stream.Collectors;

@XmlRootElement(name = "result")
public class ResultStruct {
    /**
     * Дата, вокруг которой был поиск.
     */
    @XmlElement(name = "datetime")
    public DateTimeStruct date;

    /**
     * Радиус круга, введенный во время поиска.
     */
    @XmlElement(name = "radius")
    public float radius;

    /**
     * Структура с найденным транспортом.
     */
    @XmlElementWrapper(name = "vehicles")
    @XmlElement(name = "vehicle")
    public Collection<VehicleStruct> vehicles;

    public ResultStruct() {
    }

    public ResultStruct(SearchResult result) {
        vehicles = result.getResult().stream()
                .map(VehicleStruct::new)
                .collect(Collectors.toList());

        radius = result.getRadius();

        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.setTime(result.getDate());
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int hour = cal.get(Calendar.HOUR);
        int minute = cal.get(Calendar.MINUTE);
        int second = cal.get(Calendar.SECOND);
        this.date = new DateTimeStruct(year, month, day, hour, minute, second);
    }
}
