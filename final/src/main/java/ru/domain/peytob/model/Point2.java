package ru.domain.peytob.model;

/**
 * Импортозамещенный аналог java.awt.geom.Point2D.
 */
public class Point2 {
    /**
     * Координата X.
     */
    private float x;

    /**
     * Координата Y.
     */
    private float y;

    public Point2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public Point2() {
        this(0.0f, 0.0f);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
