package ru.domain.peytob.model.exportXml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Утилитарный класс для красивого вывода даты - времени в XML, согласно
 * оговоренному формату.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class DateTimeStruct {
    /**
     * Год.
     */
    @XmlElement(name = "year")
    public int year;

    /**
     * Месяц.
     */
    @XmlElement(name = "month")
    public int month;

    /**
     * День.
     */
    @XmlElement(name = "day")
    public int day;

    /**
     * Час.
     */
    @XmlElement(name = "hour")
    public int hour;

    /**
     * Минута.
     */
    @XmlElement(name = "minute")
    public int minute;

    /**
     * Как бы странно не было, но переменная с названием second хранит в себе секунду.
     */
    @XmlElement(name = "second")
    public int second;

    public DateTimeStruct() {
    }

    public DateTimeStruct(int year, int month, int day, int hour, int minute, int second) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
}
