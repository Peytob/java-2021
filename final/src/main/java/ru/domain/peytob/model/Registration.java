package ru.domain.peytob.model;

/**
 * Регистрационный номер автомобиля.
 */
public class Registration {
    /**
     * ID в БД.
     */
    private int id;

    /**
     * Сам номер.
     */
    private String number;

    public Registration(int id, String number) {
        this.id = id;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
