package ru.domain.peytob.model;

/**
 * Тип транспорта (автобус / трамвай / ... ).
 */
public class VehicleType {
    /**
     * ID в БД.
     */
    private int id;

    /**
     * Название типа транспорта.
     */
    private String title;

    public VehicleType(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
