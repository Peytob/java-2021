package ru.domain.peytob.model.exportXml;

import ru.domain.peytob.model.Vehicle;

import javax.xml.bind.annotation.XmlElement;

/**
 * Утилитарный класс для вывода объектов класса Vehicle в XML.
 */
public class VehicleStruct {
    /**
     * Позиция.
     */
    @XmlElement(name = "position")
    public Point2Struct position;

    /**
     * Регистрационный номер.
     */
    @XmlElement(name = "registration")
    public String registration;

    /**
     * Тип транспорта (Трамвай / Троллейбус / етк).
     */
    @XmlElement(name = "title")
    public String type;

    public VehicleStruct() {
    }

    public VehicleStruct(Vehicle vehicle) {
        this.position = new Point2Struct(vehicle.getPosition());
        this.registration = vehicle.getRegistration().getNumber();
        this.type = vehicle.getType().getTitle();
    }
}
