package ru.domain.peytob.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Данные, введенные в поиск и полученные в результате поиска в БД.
 * Содержит метаданные
 */
public class SearchResult {
    /**
     * Радиус круга, введенный во время поиска.
     */
    private final float radius;

    /**
     * Дата, вокруг которой был поиск.
     */
    private final Date date;

    /**
     * Найденный транспорт.
     */
    private final ArrayList<Vehicle> result;

    public SearchResult(float radius, Date date, Collection<Vehicle> vehicles) {
        this.radius = radius;
        this.date = date;
        this.result = new ArrayList<>(vehicles);
    }

    public SearchResult(float radius, Date date) {
        this(radius, date, new ArrayList<>());
    }

    public float getRadius() {
        return radius;
    }

    public Date getDate() {
        return date;
    }

    /**
     * Добавляет автомобиль в результаты поиска.
     * Что-то типа куска функциональности условного класса
     * resultsetbuilder.
     * @param vehicle Добавляемый транспорт.
     */
    public void addVehicle(Vehicle vehicle) {
        result.add(vehicle);
    }

    public Collection<Vehicle> getResult() {
        return result;
    }
}
