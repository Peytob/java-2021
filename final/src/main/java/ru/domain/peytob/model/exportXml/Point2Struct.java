package ru.domain.peytob.model.exportXml;

import ru.domain.peytob.model.Point2;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Утилитарный класс для вывода объектов класса model.Point2 в XML.
 */
@XmlRootElement
public class Point2Struct {
    /**
     * x координата.
     */
    @XmlAttribute(name = "x")
    public float x;

    /**
     * y координата.
     */
    @XmlAttribute(name = "y")
    public float y;

    public Point2Struct() {
    }

    public Point2Struct(Point2 point) {
        this.x = point.getX();
        this.y = point.getY();
    }
}
