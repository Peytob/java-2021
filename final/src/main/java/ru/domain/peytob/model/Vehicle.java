package ru.domain.peytob.model;

import java.util.Date;

/**
 * Класс, описывающий технику. Автомобиль, автобус и так далее.
 */
public class Vehicle {
    /**
     * Тип транспортного средства.
     */
    private VehicleType type;

    /**
     * Регистрационный номер транспортного средства.
     */
    private Registration registration;

    /**
     * Координаты транспорта.
     */
    private Point2 position;

    /**
     * Момент времени, на который учтена позиция.
     */
    private Date time;

    /**
     * id в базе данных.
     */
    private int id;

    public Vehicle(int id, VehicleType type, Registration registration, Point2 position, Date time) {
        this.type = type;
        this.registration = registration;
        this.position = position;
        this.time = time;
        this.id = id;
    }

    public Vehicle(VehicleType type, Registration registration, Point2 position, Date time) {
        this(-1, type, registration, position, time);
    }

    public VehicleType getType() {
        return type;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public Registration getRegistration() {
        return registration;
    }

    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    public Point2 getPosition() {
        return position;
    }

    public void setPosition(Point2 position) {
        this.position = position;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    private String getTypeString() {
        return type.getTitle();
    }

    private String getRegistrationString() {
        return registration.getNumber();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
