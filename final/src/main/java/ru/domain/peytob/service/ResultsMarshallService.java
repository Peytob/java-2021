package ru.domain.peytob.service;

import ru.domain.peytob.model.exportXml.ResultStruct;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;

/**
 * Сервис для перевода данных, полученных из БД в XML.
 */
public class ResultsMarshallService {
    /**
     * Контекст Jaxb.
     */
    private final JAXBContext context;

    /**
     * Сереализатор для выходных данных.
     */
    private final Marshaller marshaller;

    public ResultsMarshallService() throws JAXBException {
        this.context = JAXBContext.newInstance(ResultStruct.class);
        this.marshaller = context.createMarshaller();
        this.marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    }

    /**
     * Производит сереализацию данных.
     * @param vehicles Множество автомобилей и мета-данные.
     * @return Полученная XML строка.
     * @throws JAXBException Если что-то пошло не так во время маршаллинга.
     */
    public String marshall(ResultStruct vehicles) throws JAXBException {
        StringWriter writer = new StringWriter();
        marshaller.marshal(vehicles, writer);
        return writer.toString();
    }
}
