package ru.domain.peytob.service;

import org.apache.derby.jdbc.EmbeddedDataSource;
import ru.domain.peytob.model.*;
import ru.domain.peytob.repository.VehicleRepository;

import java.sql.SQLException;
import java.util.Date;

/**
 * Сервис для работы с базой репозиторием автомобилей.
 */
public class VehicleService {
    /**
     * Целевой сервис.
     */
    private final VehicleRepository repository;

    public VehicleService(EmbeddedDataSource dataSource) throws SQLException {
        this.repository = new VehicleRepository(dataSource);
    }

    /**
     * Выбирает транспорт в радиусе 2 км. от точки Point в момент времени Time +- SELECT_EPS секунд.
     * @param point Точка, вокруг которой происходит выборка.
     * @param time Момент времени.
     * @param eps Допустимая погрешность по времени (секунды).
     * @return Множество найденных средств транспорта.
     * @throws SQLException Если что-то пошло не так.
     */
    public SearchResult selectNearby(Point2 point, Date time, int eps) throws SQLException {
        return repository.selectNearby(point, time, eps);
    }

    /**
     * Добавляет регистрацию в БД.
     * По большому счету нужна для прохождения тестов программой.
     * @param registration Регистрационный номер.
     * @return id номера.
     */
    public int insertRegistration(Registration registration) throws SQLException {
        return repository.insertRegistration(registration);
    }

    /**
     * Добавляет регистрацию в БД.
     * По большому счету нужна для прохождения тестов программой.
     * @param type Тип транспорта.
     * @return id нового типа.
     */
    public int insertType(VehicleType type) throws SQLException {
        return repository.insertType(type);
    }

    /**
     * Добавление новой записи о транспорте.
     * @param vehicle Транспорт.
     * @return id вставленного транспорта.
     * @throws SQLException Если что-то пошло не так со стороны БД.
     */
    public int insertVehicle(Vehicle vehicle) throws SQLException {
        return repository.insertVehicle(vehicle);
    }

    /**
     * Удаляет таблицы.
     * @throws SQLException Если что-то не так со стороны БД.
     */
    public void dropTables() throws SQLException {
        repository.dropTables();
    }
}
