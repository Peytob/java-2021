package ru.domain.peytob.service;

import ru.domain.peytob.model.Point2;

/**
 * Реализует в себе некоторые методы линейной алгебры.
 */
public class LinearMathService {

    /**
     * Нахождения расстояния между точками p1 и p2. Оно же - длины вектора (p1,p2).
     * @param p1 первая точка.
     * @param p2 вторая точка.
     * @return Расстояние между точками.
     */
    static public float distance(final Point2 p1, final Point2 p2) {
        float xLen = p1.getX() - p2.getX();
        float yLen = p1.getY() - p2.getY();

        return (float) Math.sqrt(xLen * xLen + yLen * yLen);
    }
}
