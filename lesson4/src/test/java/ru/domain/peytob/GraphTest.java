package ru.domain.peytob;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

// TODO Вынести задание графа в отдельные методы для того, чтобы не копировать код!

class GraphTest {

    @org.junit.jupiter.api.Test
    void addVertex() {
        Graph<Integer> graph = new Graph<>();

        Vertex<Integer> v1 = new Vertex<>(1, 15);
        Vertex<Integer> v2 = new Vertex<>(2, 25);
        Vertex<Integer> v3 = new Vertex<>(3, 100);
        Vertex<Integer> v4 = new Vertex<>(4, -52);

        assertTrue(graph.addVertex(v1));
        assertFalse(graph.addVertex(new Vertex<>(1, 10)));

        HashMap<Vertex<Integer>, HashSet<Vertex<Integer>>> expected;

        expected = new HashMap<>(Map.ofEntries(
                Map.entry(v1, new HashSet<>())
        ));
        assertEquals(expected, graph.getGraph());

        assertTrue(graph.addVertex(v2));
        assertTrue(graph.addVertex(v3));

        expected = new HashMap<>(Map.ofEntries(
                Map.entry(v1, new HashSet<>()),
                Map.entry(v2, new HashSet<>()),
                Map.entry(v3, new HashSet<>())
        ));
        assertEquals(expected, graph.getGraph());
    }

    @org.junit.jupiter.api.Test
    void deleteVertex() {
        Vertex<Integer> v1 = new Vertex<>(1, 15);
        Vertex<Integer> v2 = new Vertex<>(2, 25);
        Vertex<Integer> v3 = new Vertex<>(3, 100);
        Vertex<Integer> v4 = new Vertex<>(4, -52);

        Graph<Integer> graph = new Graph<>(new HashMap<>(Map.ofEntries(
                Map.entry(v1, new HashSet<>(List.of(v2))),
                Map.entry(v2, new HashSet<>(List.of(v1, v3))),
                Map.entry(v3, new HashSet<>(List.of(v2, v4))),
                Map.entry(v4, new HashSet<>((List.of(v3))))
        )));

        HashMap<Vertex<Integer>, HashSet<Vertex<Integer>>> expected;

        expected = new HashMap<>(Map.ofEntries(
                Map.entry(v1, new HashSet<>(List.of(v2))),
                Map.entry(v2, new HashSet<>(List.of(v1, v3))),
                Map.entry(v3, new HashSet<>(List.of(v2, v4))),
                Map.entry(v4, new HashSet<>((List.of(v3))))
        ));
        assertEquals(expected, graph.getGraph());

        assertTrue(graph.deleteVertex(v2));
        assertTrue(graph.deleteVertex(v4));

        expected = new HashMap<>(Map.ofEntries(
                Map.entry(v1, new HashSet<>()),
                Map.entry(v3, new HashSet<>())
        ));
        assertEquals(expected, graph.getGraph());

        assertFalse(graph.deleteVertex(v4));
    }

    @org.junit.jupiter.api.Test
    void addEdge() {
        Vertex<Integer> v1 = new Vertex<>(1, 15);
        Vertex<Integer> v2 = new Vertex<>(2, 25);
        Vertex<Integer> v3 = new Vertex<>(3, 100);
        Vertex<Integer> v4 = new Vertex<>(4, -52);

        Graph<Integer> graph = new Graph<>(new HashMap<>(Map.ofEntries(
                Map.entry(v1, new HashSet<>(List.of(v2))),
                Map.entry(v2, new HashSet<>(List.of(v1, v3))),
                Map.entry(v3, new HashSet<>(List.of(v2, v4))),
                Map.entry(v4, new HashSet<>((List.of(v3))))
        )));

        assertTrue(graph.addEdge(v1, v4));
        HashMap<Vertex<Integer>, HashSet<Vertex<Integer>>> expected;
        expected = new HashMap<>(Map.ofEntries(
                Map.entry(v1, new HashSet<>(List.of(v2, v4))),
                Map.entry(v2, new HashSet<>(List.of(v1, v3))),
                Map.entry(v3, new HashSet<>(List.of(v2, v4))),
                Map.entry(v4, new HashSet<>((List.of(v3, v1))))
        ));
        assertEquals(expected, graph.getGraph());
    }

    @org.junit.jupiter.api.Test
    void deleteEdge() {
        /*
            Заданный граф:
            v1 --- v2
            |      |
            |      |
            v3 --- v4

            v5 --- v6
            |       |
            |       |
            v7 --- v8
         */
        Vertex<Integer> v1 = new Vertex<>(1, 15);
        Vertex<Integer> v2 = new Vertex<>(2, 25);
        Vertex<Integer> v3 = new Vertex<>(3, 100);
        Vertex<Integer> v4 = new Vertex<>(4, -52);
        Vertex<Integer> v5 = new Vertex<>(5, -52);
        Vertex<Integer> v6 = new Vertex<>(6, -52);
        Vertex<Integer> v7 = new Vertex<>(7, -52);
        Vertex<Integer> v8 = new Vertex<>(8, -52);

        Graph<Integer> graph = new Graph<>();

        assertTrue(graph.addVertex(v1));
        assertTrue(graph.addVertex(v2));
        assertTrue(graph.addVertex(v3));
        assertTrue(graph.addVertex(v4));
        assertTrue(graph.addVertex(v5));
        assertTrue(graph.addVertex(v6));
        assertTrue(graph.addVertex(v7));
        assertTrue(graph.addVertex(v8));

        assertTrue(graph.addEdge(v1, v2));
        assertTrue(graph.addEdge(v1, v3));
        assertTrue(graph.addEdge(v3, v4));
        assertTrue(graph.addEdge(v5, v6));
        assertTrue(graph.addEdge(v5, v7));
        assertTrue(graph.addEdge(v7, v8));

        assertEquals(2, graph.getNumberOfConnectivityComponents());
        assertTrue(graph.addEdge(v1, v8));
        assertEquals(1, graph.getNumberOfConnectivityComponents());
        assertTrue(graph.deleteEdge(v1, v8));
        assertEquals(2, graph.getNumberOfConnectivityComponents());
    }

    @org.junit.jupiter.api.Test
    void getConnectivityComponentsTo() {
        Vertex<Integer> v1 = new Vertex<>(1, 15);
        Vertex<Integer> v2 = new Vertex<>(2, 25);
        Vertex<Integer> v3 = new Vertex<>(3, 100);
        Vertex<Integer> v4 = new Vertex<>(4, -52);

        Graph<Integer> graph = new Graph<>();

        assertTrue(graph.addVertex(v1));
        assertTrue(graph.addVertex(v2));

        assertEquals(2, graph.getNumberOfConnectivityComponents());

        assertTrue(graph.addEdge(v1, v2));
        assertEquals(1, graph.getNumberOfConnectivityComponents());

        assertTrue(graph.addVertex(v3));
        assertEquals(2, graph.getNumberOfConnectivityComponents());

        assertTrue(graph.deleteVertex(v3));
        assertEquals(1, graph.getNumberOfConnectivityComponents());

        assertTrue(graph.addVertex(v4));
        assertTrue(graph.addVertex(v3));
        assertTrue(graph.addEdge(v3, v4));
        assertEquals(2, graph.getNumberOfConnectivityComponents());

        assertTrue(graph.addEdge(v2, v3));
        assertEquals(1, graph.getNumberOfConnectivityComponents());

        assertTrue(graph.deleteEdge(v2, v3));
        assertEquals(2, graph.getNumberOfConnectivityComponents());
    }

    @Test
    void achievableVertexes() {
        /*
            Заданный граф:
            v1 --- v2
            |      |
            |      |
            v3 --- v4

            v5 --- v6
            |       |
            |       |
            v7 --- v8
         */
        Vertex<Integer> v1 = new Vertex<>(1, 15);
        Vertex<Integer> v2 = new Vertex<>(2, 25);
        Vertex<Integer> v3 = new Vertex<>(3, 100);
        Vertex<Integer> v4 = new Vertex<>(4, -52);
        Vertex<Integer> v5 = new Vertex<>(5, -52);
        Vertex<Integer> v6 = new Vertex<>(6, -52);
        Vertex<Integer> v7 = new Vertex<>(7, -52);
        Vertex<Integer> v8 = new Vertex<>(8, -52);

        Graph<Integer> graph = new Graph<>();

        assertTrue(graph.addVertex(v1));
        assertTrue(graph.addVertex(v2));
        assertTrue(graph.addVertex(v3));
        assertTrue(graph.addVertex(v4));
        assertTrue(graph.addVertex(v5));
        assertTrue(graph.addVertex(v6));
        assertTrue(graph.addVertex(v7));
        assertTrue(graph.addVertex(v8));

        assertTrue(graph.addEdge(v1, v2));
        assertTrue(graph.addEdge(v1, v3));
        assertTrue(graph.addEdge(v3, v4));
        assertTrue(graph.addEdge(v5, v6));
        assertTrue(graph.addEdge(v5, v7));
        assertTrue(graph.addEdge(v7, v8));

        assertEquals(2, graph.getNumberOfConnectivityComponents());

        assertEquals(new HashSet<>(List.of(v1, v2, v3, v4)), graph.achievableVertexes(v1));
        assertEquals(new HashSet<>(List.of(v5, v6, v7, v8)), graph.achievableVertexes(v5));
    }

    @Test
    void getById() {
        Vertex<Integer> v1 = new Vertex<>(1, 15);
        Vertex<Integer> v2 = new Vertex<>(2, 25);
        Vertex<Integer> v3 = new Vertex<>(3, 100);
        Vertex<Integer> v4 = new Vertex<>(4, -52);

        Graph<Integer> graph = new Graph<>();
        assertTrue(graph.addVertex(v1));
        assertTrue(graph.addVertex(v2));
        assertTrue(graph.addVertex(v3));
        assertTrue(graph.addVertex(v4));

        assertEquals(v4, graph.getById(4));
        assertEquals(v1, graph.getById(1));
        boolean expCatch = false;
        try {
            graph.getById(150);
        } catch (NoSuchElementException exp) {
            expCatch = true;
        }
        assertTrue(expCatch);
    }
}
