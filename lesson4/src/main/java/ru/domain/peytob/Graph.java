package ru.domain.peytob;

import java.util.*;

/**
 * Граф, содержащий в себе информацию о вершинах (узлах) и ребрах (связях) между ними. Неорентированный.
 * Граф хранится в формате списка смежности.
 * <p>
 * Просчет изменений в компонентах связности происходит сразу же при добавлении новой вершины (автоматически
 * добавляется +1 компонента), удалении вершины (удаляется из ее компоненты и компонента проверяется на
 * пустоту), добавлении ребра (проверяется соединение двух компонент в одну), удалении ребра (проверяется
 * разбитие одной компоненты на N более малых).
 *
 * @param <T> Тип данных в графе и его узлах.
 */
public class Graph<T> {
    /**
     * Список смежности графа.
     */
    HashMap<Vertex<T>, HashSet<Vertex<T>>> graph;

    /**
     * Список всех компонент смежности графа.
     */
    ArrayList<HashSet<Vertex<T>>> connectivityComponents;

    public Graph(HashMap<Vertex<T>, HashSet<Vertex<T>>> graph) {
        this.graph = graph;
        this.connectivityComponents = new ArrayList<>();
    }

    public Graph() {
        this(new HashMap<>());
    }

    /**
     * Добавляет вершину в граф, либо ничего не делает, если вершина уже есть (проверка по методу equals()).
     *
     * @param vertex Вершина.
     * @return true, если была добавлена новая вершина.
     */
    public boolean addVertex(Vertex<T> vertex) {
        if (graph.containsKey(vertex))
            return false;

        graph.put(vertex, new HashSet<>());
        connectivityComponents.add(new HashSet<>(List.of(vertex)));
        return true;
    }

    /**
     * Удаляет вершину из графа, либо ничего не делает, если вершина уже есть (проверка по методу equals()).
     *
     * @param vertex Вершина.
     * @return true, если была удалена вершина.
     */
    public boolean deleteVertex(Vertex<T> vertex) {
        if (!graph.containsKey(vertex))
            return false;

        graph.forEach((key, value) -> {
            value.removeIf(el -> el.equals(vertex));
        });

        graph.remove(vertex);

        connectivityComponents.forEach(component -> {
            component.removeIf(el -> el.equals(vertex));
        });

        connectivityComponents.removeIf(HashSet::isEmpty);

        return true;
    }

    /**
     * Добавляет ребро в граф, либо ничего не делает, если ребро уже есть.
     *
     * @param first  Первая вершина.
     * @param second Вторая вершина.
     * @return true, если было добавлено ребро.
     */
    public boolean addEdge(Vertex<T> first, Vertex<T> second) {
        if (!graph.containsKey(first) || !graph.containsKey(second))
            return false;

        graph.get(first).add(second);
        graph.get(second).add(first);

        HashSet<Vertex<T>> firstComponent = getConnectivityComponentsTo(first);
        HashSet<Vertex<T>> secondComponent = getConnectivityComponentsTo(second);

        // Вершины из разных компонетов связности - их нужно запихать в 1.
        if (firstComponent != secondComponent) {
            HashSet<Vertex<T>> resultComponent = new HashSet<>();
            resultComponent.addAll(firstComponent);
            resultComponent.addAll(secondComponent);
            connectivityComponents.remove(firstComponent);
            connectivityComponents.remove(secondComponent);
            connectivityComponents.add(resultComponent);
        }

        return true;
    }

    /**
     * Удаляет ребро из графа, либо ничего не делает, если ребра не существует.
     *
     * @param first  Первая вершина.
     * @param second Вторая вершина.
     * @return true, если было удалено ребро.
     */
    public boolean deleteEdge(Vertex<T> first, Vertex<T> second) {
        if (!graph.containsKey(first) || !graph.containsKey(second))
            return false;

        graph.get(first).remove(second);
        graph.get(second).remove(first);

        // Они в 1 комп. тк между ними ребро
        ArrayList<Vertex<T>> component = new ArrayList<>(getConnectivityComponentsTo(first));
        ArrayList<HashSet<Vertex<T>>> newComponents = new ArrayList<>();
        while (!component.isEmpty()) {
            HashSet<Vertex<T>> part = achievableVertexes(component.get(0));
            newComponents.add(part);
            component.removeAll(part);
        }

        if (newComponents.size() != 1) {
            connectivityComponents.remove(getConnectivityComponentsTo(first));
            connectivityComponents.addAll(newComponents);
        }

        return true;
    }

    /**
     * Возвращает количество компонент связности в графе.
     *
     * @return Количество компонент связности в графе.
     */
    public int getNumberOfConnectivityComponents() {
        return connectivityComponents.size();
    }

    public ArrayList<HashSet<Vertex<T>>> getConnectivityComponents() {
        return connectivityComponents;
    }

    public HashMap<Vertex<T>, HashSet<Vertex<T>>> getGraph() {
        return graph;
    }

    /**
     * Поиск компоненты связности, в которую входит вершина vertex.
     *
     * @param vertex Вершина, для которой осуществляется поиск.
     * @return Компонент связности, если вершина была найдена. Иначе - null.
     */
    private HashSet<Vertex<T>> getConnectivityComponentsTo(Vertex<T> vertex) {
        for (HashSet<Vertex<T>> entry : connectivityComponents)
            for (Vertex<T> v : entry)
                if (v.equals(vertex))
                    return entry;

        return null;
    }

    /**
     * Возвращает массив вершин, достижимых из startVertex.
     * TODO Отдельный класс для алгоритмов на графе (хотя он тут 1, но ладно).
     * @param startVertex Начальняа вершина для обхода.
     * @return Множество вершин, достижимых при обходе.
     */
    public HashSet<Vertex<T>> achievableVertexes(Vertex<T> startVertex) {
        HashSet<Vertex<T>> acc = new HashSet<>();

        Stack<Vertex<T>> stack = new Stack<>();
        stack.add(startVertex);

        while (!stack.empty()) {
            Vertex<T> vertex = stack.pop();
            acc.add(vertex);

            for (Vertex<T> neighbor : graph.get(vertex)) {
                if (!acc.contains(neighbor))
                    stack.push(neighbor);
            }
        }

        return acc;
    }

    public Vertex<T> getById(int vertexId) {
        return graph.keySet().stream().filter(v -> v.getId() == vertexId).findAny().get();
    }
}
