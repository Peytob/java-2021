package ru.domain.peytob;

import java.util.Objects;

/**
 * Вершина (узел) графа.
 * @param <T> Тип данных в узле.
 */
public class Vertex<T> {
    /**
     * Данные узла.
     */
    private T data;

    /**
     * Номер узла.
     */
    private final int id;

    public Vertex(int id)
    {
        this(id, null);
    }

    public Vertex(int id, T data)
    {
        this.data = data;
        this.id = id;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex<?> vertex = (Vertex<?>) o;
        return id == vertex.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
