/**
 * Класс зоопарка, хранящий всех сотрудников, животных и вольерах.
 */
public class Zoo {
    /**
     * Множество вольеров. Неизменное количество.
     */
    private final Aviary[] aviaries;

    /**
     * Множество животных.
     */
    private Animal[] animals;

    /**
     * Множество рабочих. Неизменное количество.
     */
    private final Employee[] employees;

    /**
     * Устанавливает начальное заполнение зоопарка.
     * При добавлении животного оно попадает к сотруднику,
     * имеющему наименьшее количество животных, тк
     * в задаче не было указано как именно выбирать сотрудника
     * для животинок и наиболее свободный
     * загон. (пользователь или сама система)
     */
    public Zoo() {
        employees = new Employee[]{
                new Employee("Ваня", 27),
                new Employee("Геннадий", 32),
                new Employee("Антон Павлович", 54)
        };

        aviaries = new Aviary[]{
                new Aviary(1),
                new Aviary(7),
                new Aviary(19),
                new Aviary(42),
        };

        animals = new Animal[0];

        addAnimal("Жираф Антон");
        addAnimal("Енот");
        addAnimal("Хомяк хома");
        addAnimal("Хомяк Екатерина");
        addAnimal("Кот Ваня");
    }

    /**
     * Добавляет новое животное с заданным именем. Система сама находит ему
     * вальер и рабочего.
     *
     * @param animalName Имя животного.
     * @return Новое животное.
     */
    public Animal addAnimal(String animalName) {
        if (animalName == null)
            return null; // Тут должно быть исключение, но ладно

        // Поиск самого свободного сотрудника

        final int[] empAnimals = new int[employees.length];

        for (Animal animal : animals) {
            Employee target = animal.getEmployee();

            int index = 0;
            for (Employee emp : employees) {
                // проверка через == тк тут используются только ссылки
                if (emp == target) {
                    empAnimals[index]++;
                    break;
                }

                index++;
            }
        }

        int freeEmployeeIndex = 0;
        int min = empAnimals[0];
        for (int i = 1; i < empAnimals.length; ++i) {
            if (empAnimals[i] < min) {
                min = empAnimals[i];
                freeEmployeeIndex = i;
            }
        }

        // Поиск самого свободного вальера

        final int[] aviAnimals = new int[employees.length];

        for (Animal animal : animals) {
            Aviary target = animal.getAviary();

            int index = 0;
            for (Aviary aviary : aviaries) {
                // проверка через == тк тут используются только ссылки
                if (aviary == target) {
                    aviAnimals[index]++;
                    break;
                }

                index++;
            }
        }

        int freeAviaryIndex = 0;
        min = aviAnimals[0];
        for (int i = 1; i < aviAnimals.length; ++i) {
            if (aviAnimals[i] < min) {
                min = aviAnimals[i];
                freeAviaryIndex = i;
            }
        }

        // Добавление нового зверька

        Animal newAnimal = new Animal(animalName, employees[freeEmployeeIndex], aviaries[freeAviaryIndex]);
        Animal[] newAnimals = new Animal[animals.length + 1];
        System.arraycopy(animals, 0, newAnimals, 0, animals.length);

        newAnimals[newAnimals.length - 1] = newAnimal;
        animals = newAnimals;

        return newAnimal;
    }

    /**
     * Удаление животного (продажа / гибель).
     *
     * @param animal Удаляемое животное.
     * @return Ссылка на удаленное животное.
     */
    public Animal deleteAnimal(Animal animal) {
        int index = -1; // Индекс искомого животного
        for (int i = 0; i < animals.length; ++i)
            if (animals[i] == animal) {
                index = i;
                break;
            }

        if (index == -1)
            return null;

        Animal[] newAnimals = new Animal[animals.length - 1];
        for (int i = 0, j = 0; i < animals.length; ++i) {
            if (i != index) {
                newAnimals[j] = animals[i];
                j++;
            }
        }

        animals = newAnimals;
        return animal;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();

        builder.append(String.format("%25s%25s%8s\n", "Животное", "Работник", "Вольер"));

        for (Animal an : animals) {
            final Employee emp = an.getEmployee();
            final Aviary avi = an.getAviary();

            String row = String.format("%25s%25s%8d\n", an, emp, avi.getNumber());
            builder.append(row);
        }

        return builder.toString();
    }
}