import java.util.Date;

/**
 * Болезнь животного.
 */
public class Sick {
    /**
     * Название болезни.
     */
    private String name;

    /**
     * Дата начала болезни.
     */
    private Date beginDate;

    /**
     * Дата окончания болезни. Если болезнь в процессе - null.
     */
    private Date endDate;

    public Sick(String name, Date beginDate) {
        this.name = name;
        this.beginDate = beginDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        if (endDate != null)
            return "" + name + " от " + beginDate + " до " + endDate;

        else
            return "" + name + " от " + beginDate;
    }
}
