import java.util.Arrays;
import java.util.Date;

public class Application {
    public static void main(String[] args) {
        Zoo zoo = new Zoo();

        System.out.println(zoo.toString());

        System.out.println("Добавление нового животного - Коровки Буренки");
        Animal cow = zoo.addAnimal("Корова Буренка");
        System.out.println(zoo);

        System.out.println("Добавление нового животного - Собачки Стрелки");
        Animal dog = zoo.addAnimal("Собачка Стрелка");
        System.out.println(zoo);

        System.out.println("Коровку продали и она исчезла из зоопарка");
        zoo.deleteAnimal(cow);
        System.out.println(zoo);

        System.out.println("Собачка заболела гриппом и астеохондрозом!");
        dog.getCold(new Sick("Коровий грипп", new Date(1212121212121L)));
        dog.getCold(new Sick("Астеохондроз", new Date(1212121212121L)));
        System.out.println(Arrays.toString(dog.getSickHistory()));

        System.out.println("Собачка частично выздоровела!");
        dog.getWell(new Date(1212121212121L + 60 * 60 * 24 * 7 * 1000L), "Астеохондроз");
        System.out.println(Arrays.toString(dog.getSickHistory()));

        System.out.println("Произведем очистку вальера собаки");
        dog.getAviary().addClearDate(new Date(1212121212121L - 60 * 60 * 24 * 7 * 1000L));
        System.out.println(Arrays.toString(dog.getAviary().getClearDates()));
    }
}
