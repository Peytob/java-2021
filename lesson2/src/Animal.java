import java.util.Date;

/**
 * Класс животного.
 */
public class Animal {
    /**
     * Привязанный к животному сотрудник.
     */
    private final Employee employee;

    /**
     * Привязанный вальер.
     */
    private final Aviary aviary;

    /**
     * Имя животного.
     */
    private final String name;

    /**
     * История болезни.
     */
    private Sick[] sickHistory;

    public Animal(String name, Employee employee, Aviary aviary) {
        this.name = name;
        this.employee = employee;
        this.aviary = aviary;
        this.sickHistory = new Sick[0];
    }

    public Employee getEmployee() {
        return employee;
    }

    public Aviary getAviary() {
        return aviary;
    }

    public String getName() {
        return name;
    }

    public Sick[] getSickHistory() {
        return sickHistory;
    }

    /**
     * Выздоравление животного от последней полезни.
     *
     * @param endSickDate Дата выздоровления.
     * @param sickName    имя болезни
     */
    public void getWell(Date endSickDate, String sickName) {
        for (Sick sick : sickHistory) {
            if (sick.getName().equals(sickName)) {
                sick.setEndDate(endSickDate);
                return;
            }
        }
    }

    /**
     * Добавление новой болезни
     *
     * @param sick Новая болезнь.
     */
    public void getCold(Sick sick) {
        Sick[] newSickHistory = new Sick[sickHistory.length + 1];
        System.arraycopy(sickHistory, 0, newSickHistory, 0, sickHistory.length);
        newSickHistory[newSickHistory.length - 1] = sick;
        sickHistory = newSickHistory;
    }

    @Override
    public String toString() {
        return name;
    }
}