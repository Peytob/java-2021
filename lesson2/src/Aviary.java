import java.util.Date;

/**
 * Класс вольера. Хранит в себе зверька и информацию об уборках.
 */
public class Aviary {
    /**
     * Номер вальера.
     */
    private final int number;

    private Date[] clearDates;

    public Aviary(int number) {
        this.clearDates = new Date[0];
        this.number = number;
    }

    /**
     * Номер вальера.
     *
     * @return Номер.
     */
    public int getNumber() {
        return number;
    }

    /**
     * Пометка о чистке вальера.
     *
     * @param date Дата чистки.
     */
    public void addClearDate(Date date) {
        Date[] newClearDatesNew = new Date[clearDates.length + 1];
        System.arraycopy(clearDates, 0, newClearDatesNew, 0, clearDates.length);
        newClearDatesNew[newClearDatesNew.length - 1] = date;
        clearDates = newClearDatesNew;
    }

    /**
     * Получение массива дат очистки вальера.
     * @return Массива дат очистки вальера
     */
    public Date[] getClearDates() {
        return clearDates;
    }

    @Override
    public String toString() {
        return Integer.toString(number);
    }
}