/**
 * Класс сотрудника. Хранит в себе информацию о рабочем
 */
public class Employee {
    /**
     * Имя сотрудника.
     */
    private final String name;

    /**
     * Возраст сотрудника.
     */
    private final int age;

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return name;
    }
}
