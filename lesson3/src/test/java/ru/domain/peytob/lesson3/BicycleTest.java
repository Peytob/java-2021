package ru.domain.peytob.lesson3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.*;

class BicycleTest {
    @org.junit.jupiter.api.Test
    @DisplayName("Проверка того, что корректно проверяются повреждения у некоторого конкретного индивидуального транспорта")
    void isDamaged() {
        Vehicle target = new Bicycle("BicycleProm", "Ural", 15, Material.Plastic);
        target.breakSomething();
        assertTrue(target.isDamaged());

        target.repair();
        assertFalse(target.isDamaged());
    }
}
