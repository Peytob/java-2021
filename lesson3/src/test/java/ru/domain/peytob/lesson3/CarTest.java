package ru.domain.peytob.lesson3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CarTest {

    @Test
    void isDamaged() {
        Vehicle target = new Car("AirCorp", "WingBreaker", 1500, 160);
        target.breakSomething();
        assertTrue(target.isDamaged());

        ((Car) target).damageTire();
        assertTrue(target.isDamaged());

        target.repair();
        assertTrue(target.isDamaged());

        ((Car) target).repairTire();
        assertFalse(target.isDamaged());
    }
}