package ru.domain.peytob.lesson3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelicopterTest {

    @Test
    void isDamaged() {
        Vehicle target = new Helicopter("AirCorp", "WingBreaker", 1500, 3000);
        target.breakSomething();
        assertTrue(target.isDamaged());

        ((Helicopter) target).damageScrew();
        assertTrue(target.isDamaged());

        target.repair();
        assertTrue(target.isDamaged());

        ((Helicopter) target).repairScrew();
        assertFalse(target.isDamaged());
    }
}