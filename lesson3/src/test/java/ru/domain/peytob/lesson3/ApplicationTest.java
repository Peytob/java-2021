package ru.domain.peytob.lesson3;

import jdk.jfr.Description;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class ApplicationTest {

    @org.junit.jupiter.api.Test
    @DisplayName("Тестирование работы ренты")
    void test()
    {
        Vehicle first = new Horse("1", "2", 1, Horse.Breed.Desert);
        assertFalse(first.isRent());
        first.setRent(new Rent(new Date()));
        assertTrue(first.isRent());
    }
}