package ru.domain.peytob.lesson3;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlaneTest {

    @Test
    void isDamaged() {
        Vehicle target = new Plane("AirCorp", "WingBreaker", 1500, 700);
        target.breakSomething();
        assertTrue(target.isDamaged());

        ((Plane) target).damageWing();
        assertTrue(target.isDamaged());

        target.repair();
        assertTrue(target.isDamaged());

        ((Plane) target).repairWing();
        assertFalse(target.isDamaged());
    }

    @Test
    void isWingAlive() {
        Plane plane = new Plane("test", "2021", 15, 51);
        assertFalse(plane.isDamaged());
        assertTrue(plane.isWingAlive());
        plane.damageWing();
        assertTrue(plane.isDamaged());
        assertFalse(plane.isWingAlive());
        plane.repairWing();
        assertFalse(plane.isDamaged());
        assertTrue(plane.isWingAlive());
    }
}
