package ru.domain.peytob.lesson3;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        ArrayList<Vehicle> vehicles = new ArrayList<>();

        vehicles.add(new Helicopter("Вертолетик", "MK35", 400, 1200));
        vehicles.add(new Horse("Лошадь", "Королевствая", 320, Horse.Breed.Royal));
        vehicles.add(new Car("ВАЗ", "500", 150, 150));

        String message = "1. Поменить как арендованный на 1 день, 2 - снять аренду, 3 - выйти: ";
        Scanner scan = new Scanner(System.in);
        int inp;
        do
        {
            int index = 1;
            for (Vehicle v : vehicles) {
                System.out.println("#" + index + "; " + v);
                index++;
            }
            System.out.print(message);
            inp = scan.nextInt();

            if (inp == 1) {
                System.out.print("Введите номер элемента: ");
                int ind = scan.nextInt();
                if (ind <= vehicles.size() && ind >= 0)
                    vehicles.get(ind - 1).setRent(new Rent(new Date()));

                else
                    System.out.println("Странное значение, да.");
            }

            else if (inp == 2)
            {
                System.out.print("Введите номер элемента: ");
                int ind = scan.nextInt();
                if (ind <= vehicles.size() && ind >= 0)
                    vehicles.get(ind - 1).unRent();

                else
                    System.out.println("Странное значение, да.");
            }
        } while (inp != 3);
    }
}
