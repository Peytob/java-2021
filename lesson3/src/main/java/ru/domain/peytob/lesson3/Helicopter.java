package ru.domain.peytob.lesson3;

/**
 * Вертолет, имеющий макчимальную высоту полета. Может сломаться винт.
 */
public class Helicopter extends MotorVehicle {
    /**
     * Максимальная высота полета.
     */
    int maximalHeight;

    /**
     * Поврежден ли винт?
     */
    boolean isScrewAlive;

    public Helicopter(String _model, String _mark, int _power, int _maximalHeight) {
        super(_model, _mark, true, _power);
        maximalHeight = _maximalHeight;
    }

    @Override
    public boolean isDamaged() {
        return !isScrewAlive || isDamaged;
    }

    public int getMaximalHeight() {
        return maximalHeight;
    }

    public void setMaximalHeight(int maximalHeight) {
        this.maximalHeight = maximalHeight;
    }

    public boolean isScrewAlive() {
        return isScrewAlive;
    }

    /**
     * Поломка винта
     */
    public void damageScrew()
    {
        isScrewAlive = false;
    }

    /**
     * Починка винта
     */
    public void repairScrew()
    {
        isScrewAlive = true;
    }

    @Override
    public String toString() {
        return "Вертолет, " + model + " " + mark + ", " + power + " лс, " + (isDamaged() ? "цел" : "сломан")
                + ", " + (isRent() ? "арендован" : "не арендован");
    }
}
