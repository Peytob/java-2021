package ru.domain.peytob.lesson3;

import java.util.Date;

public class Rent {
    Date begin;

    public Rent(Date _begin)
    {
        begin = _begin;
    }

    public Date getBegin() {
        return begin;
    }
}
