package ru.domain.peytob.lesson3;

/**
 * Автомобиль. Имеет максимальную скорость. Может сломаться шина.
 */
public class Car extends MotorVehicle {
    /**
     * Максимальная скорость автомобиля.
     */
    int maximalSpeed;

    /**
     * Все ли хорошо с шинами?
     */
    boolean isTireAlive;

    public Car(String _model, String _mark, int _power, int _maximalSpeed) {
        super(_model, _mark, false, _power);
        maximalSpeed = _maximalSpeed;
        isTireAlive = true;
    }

    /**
     * Поломка шин.
     */
    public void damageTire()
    {
        isTireAlive = false;
    }

    /**
     * Починка шин
     */
    public void repairTire()
    {
        isTireAlive = true;
    }

    @Override
    public boolean isDamaged() {
        return isDamaged || !isTireAlive;
    }

    public int getMaximalSpeed() {
        return maximalSpeed;
    }

    public boolean isTireAlive() {
        return isTireAlive;
    }

    @Override
    public String toString() {
        return "Автомобиль, " + model + " " + mark + ", " + power + " лс, " + (isDamaged() ? "цел" : "сломан")
                + ", " + (isRent() ? "арендован" : "не арендован");
    }
}
