package ru.domain.peytob.lesson3;

/**
 * Самокат. Имеет свой материал и флаг наличия освещения
 */
public class Scooter extends IndividualVehicle {
    boolean hasLight;
    Material material;

    public Scooter(String _model, String _mark, int _weight, Material _material, boolean _light) {
        super(_model, _mark, _weight);
        hasLight = _light;
        material = _material;
    }

    public boolean isHasLight() {
        return hasLight;
    }

    public Material getMaterial() {
        return material;
    }

    @Override
    public String toString() {
        return "Самокат, " + model + " " + mark + ", " + weight + " кг, " + (isDamaged() ? "цел" : "сломан")
                + ", " + (isRent() ? "арендован" : "не арендован");
    }
}
