package ru.domain.peytob.lesson3;

/**
 * В некоторой степени лошади тоже являются личным транспортом.
 */
public class Horse extends IndividualVehicle {
    /**
     * Породы лошадей.
     */
    public enum Breed {
        White, Royal, Fasted, Desert, Russian, BlackLegs
    }

    Breed breed;

    public Horse(String _model, String _mark, int _weight, Breed _breed) {
        super(_model, _mark, _weight);
        breed = _breed;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    @Override
    public String toString() {
        return "Лошадка, " + model + " " + mark + ", " + weight + " кг, " + (isDamaged() ? "здорова" : "больна")
                + ", " + (isRent() ? "арендован" : "не арендован");
    }
}
