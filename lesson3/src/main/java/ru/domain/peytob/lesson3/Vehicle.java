package ru.domain.peytob.lesson3;

import java.util.Date;

/**
 * Общий класс для любых транспортных средств.
 */
public abstract class Vehicle {
    /**
     * Модель транспортного средства.
     */
    protected final String model;

    /**
     * Марка транспортного средства.
     */
    protected final String mark;

    /**
     * Может ли транспортное средство летать (должно быть предусмотрено его
     * конструкцией)
     */
    protected final boolean canFly;

    /**
     * Флаг поврежденности транспортного средства.
     */
    boolean isDamaged;

    /**
     * Арендован ли транспорт?
     * Если не арендован - возможно значение null.
     */
    Rent rent;

    public Vehicle(String _model, String _mark, boolean _canFly)
    {
        model = _model;
        mark = _mark;
        canFly = _canFly;
    }

    /**
     * Повреждение транспорта.
     */
    public void breakSomething()
    {
        isDamaged = true;
    }

    /**
     * Починка транспорта.
     */
    public void repair()
    {
        isDamaged = false;
    }

    public String getModel() {
        return model;
    }

    public String getMark() {
        return mark;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public abstract boolean isDamaged();

    public Rent getRent() {
        return rent;
    }

    public void setRent(Rent rent) {
        this.rent = rent;
    }

    /**
     * Проверка на то, что транспортное средство арендовано.
     * @return True, если под арендой.
     */
    public boolean isRent()
    {
        return rent != null;
    }

    public void unRent()
    {
        rent = null;
    }
}
