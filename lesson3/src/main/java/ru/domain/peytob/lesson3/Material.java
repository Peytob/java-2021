package ru.domain.peytob.lesson3;

/**
 * Материал, из которого может быть сделано транспортное средство.
 */
public enum Material {
    Wood, Iron, Steel, Plastic
}
