package ru.domain.peytob.lesson3;

/**
 * Транспортное средство, имеющее двигатель (или несколько) с какой-либо мощностью.
 * Имеют уникальные повреждения, в отличии от индивидуального транспорта.
 */
public abstract class MotorVehicle extends Vehicle {
    int power;

    public MotorVehicle(String _model, String _mark, boolean _canFly, int _power) {
        super(_model, _mark, _canFly);
        power = _power;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
