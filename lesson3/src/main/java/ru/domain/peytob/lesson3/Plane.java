package ru.domain.peytob.lesson3;

/**
 * Самолет. Может летать и имеет дальность полета. Может сломаться крыло.
 */
public class Plane extends MotorVehicle {
    /**
     * Дальность полета.
     */
    int flightRange;

    /**
     * Все ли в порядке с крыльями
     */
    boolean isWingAlive;

    public Plane(String _model, String _mark, int _power, int _flightRange) {
        super(_model, _mark, true, _power);
        flightRange = _flightRange;
        isWingAlive = true;
    }

    @Override
    public boolean isDamaged() {
        return isDamaged || !isWingAlive;
    }

    /**
     * Поломка крыльев
     */
    public void damageWing()
    {
        isWingAlive = false;
    }

    /**
     * Починка крыльев
     */
    public void repairWing()
    {
        isWingAlive = true;
    }

    public int getFlightRange() {
        return flightRange;
    }

    public boolean isWingAlive() {
        return isWingAlive;
    }

    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    @Override
    public String toString() {
        return "Самолет, " + model + " " + mark + ", " + power + " лс, " + (isDamaged() ? "цел" : "сломан")
                + ", " + (isRent() ? "арендован" : "не арендован");
    }
}
