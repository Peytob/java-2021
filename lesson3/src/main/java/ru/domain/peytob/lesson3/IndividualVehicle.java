package ru.domain.peytob.lesson3;

/**
 * Транспортное средство, которое не имеет мотора.
 * Каждое индивидуальное транспортное средство не требует специального
 * ремонта.
 */
public abstract class IndividualVehicle extends Vehicle {

    /**
     * Вес транспортного средства.
     */
    int weight;

    public IndividualVehicle(String _model, String _mark, int _weight) {
        super(_model, _mark, false);
        weight = _weight;
        isDamaged = false;
    }

    @Override
    public boolean isDamaged() {
        return isDamaged;
    }
}
