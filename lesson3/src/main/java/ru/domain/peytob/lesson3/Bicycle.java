package ru.domain.peytob.lesson3;

/**
 * Велосипед. Отличается тем, что имеет свой материал рамы.
 */
public class Bicycle extends IndividualVehicle {

    /**
     * Материал рамы велосипеда.
     */
    Material material;

    public Bicycle(String _model, String _mark, int _weight, Material _material) {
        super(_model, _mark, _weight);
        material = _material;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Велосепед, " + model + " " + mark + ", " + weight + " кг, " + (isDamaged() ? "цел" : "сломан")
                + ", " + (isRent() ? "арендован" : "не арендован");
    }
}
