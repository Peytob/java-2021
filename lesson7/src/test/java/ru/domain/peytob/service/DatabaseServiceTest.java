package ru.domain.peytob.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.domain.peytob.databaseProvider.DerbyProvider;
import ru.domain.peytob.model.Student;
import ru.domain.peytob.repository.StudentRepository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Реализация DatabaseService полностью покрывает тестами и репозиторий.
 * По большому счету, тут реализуется паттерн "Стратегия" - репозиторий просто
 * определяет поведение сервиса. Сервис везде просто вызывает его методы.
 */
class DatabaseServiceTest {
    DerbyProvider provider;

    @BeforeEach
    void setUp() throws IOException, SQLException {
        provider = new DerbyProvider();

        StudentRepository repository = new StudentRepository(provider.getDataSource(), "Students");
        DatabaseService service = new DatabaseService(repository);

        service.dropTable("Students");
    }

    @Test
    void selectAll() throws SQLException {
        StudentRepository repository = new StudentRepository(provider.getDataSource(), "Students");
        DatabaseService service = new DatabaseService(repository);

        Student s1 = new Student(1,
                "Vlad",
                23,
                new Date(),
                true,
                new Date()
                );

        Student s2 = new Student(2,
                "Ivan",
                25,
                new Date(),
                false,
                new Date()
        );

        Student s3 = new Student(3,
                "Anastasiya",
                20,
                new Date(),
                true,
                new Date()
        );

        final TreeSet<Student> expected = new TreeSet<>();
        service.insert(s1);
        expected.add(s1);
        service.insert(s2);
        expected.add(s2);
        service.insert(s3);
        expected.add(s3);

        Collection<Student> students = service.selectAll();

        assertEquals(expected, students);
        service.dropTable("Students");
    }

    @Test
    void delete() throws SQLException {
        StudentRepository repository = new StudentRepository(provider.getDataSource(), "Students");
        DatabaseService service = new DatabaseService(repository);

        Student s1 = new Student(1,
                "Deletovich",
                11,
                new Date(),
                false,
                new Date()
        );

        Student s2 = new Student(2,
                "Insertov",
                13,
                new Date(),
                false,
                new Date()
        );

        Student s3 = new Student(3,
                "Updatov",
                10,
                new Date(),
                true,
                new Date()
        );

        final TreeSet<Student> expected = new TreeSet<>();
        service.insert(s1);
        expected.add(s1);
        service.insert(s2);
        expected.add(s2);
        service.insert(s3);
        expected.add(s3);

        service.delete(s2);
        expected.remove(s2);

        assertEquals(expected, service.selectAll());
        service.dropTable("Students");
    }

    @Test
    void update() throws SQLException {
        StudentRepository repository = new StudentRepository(provider.getDataSource(), "Students");
        DatabaseService service = new DatabaseService(repository);

        Student s1 = new Student(1,
                "Old",
                11,
                new Date(),
                false,
                new Date()
        );

        final TreeSet<Student> expected = new TreeSet<>();
        service.insert(s1);
        s1.setName("New");
        service.update(s1);
        expected.add(s1);

        assertEquals(expected, service.selectAll());
        service.dropTable("Students");
    }
}
