package ru.domain.peytob;

import ru.domain.peytob.databaseProvider.DerbyProvider;
import ru.domain.peytob.model.Student;
import ru.domain.peytob.repository.StudentRepository;

import java.io.IOException;
import java.sql.*;
import java.util.Collection;

public class Application {
    public static void main(String[] args) throws SQLException {
        DerbyProvider provider;
        try {
            provider = new DerbyProvider();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return;
        }

        StudentRepository repository;
        try {
            repository = new StudentRepository(provider.getDataSource(), "Students");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return;
        }

//        try {
//            repository.insert(new Student(-1, "Ivan", 21, new Date(), false, new Date()));
//        } catch (SQLException e) {
//            System.out.println(e.getMessage());
//        }

        Collection<Student> students = null;
        try {
            students = repository.selectAll();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        assert students != null;
        for (Student st : students) {
            System.out.println(st.getId());
            System.out.println(st.getName());
            System.out.println(st.getAge());
            System.out.println();
        }
    }
}
