package ru.domain.peytob.repository;

import org.apache.derby.jdbc.EmbeddedDataSource;

import ru.domain.peytob.model.Student;

import java.sql.*;
import java.util.Collection;
import java.util.TreeSet;

/**
 * Репозиторий для студентов. Реализует базовые CRUD операции и сброс базы.
 */
public class StudentRepository {
    /**
     * Фактически, пул подключений.
     */
    private final EmbeddedDataSource dataSource;

    /**
     * Название таблицы.
     */
    private final String table;

    public StudentRepository(EmbeddedDataSource dataSource, String table) throws SQLException {
        this.dataSource = dataSource;
        this.table = table;

        if (!isTableExists(this.table)) {
            initializeTables();
        }
    }

    /**
     * Проверка того, что таблица существует.
     * @param tableName Название таблицы.
     * @return True, если такая таблица уже есть.
     * @throws SQLException Если была ошибка во время SQL операций.
     */
    private boolean isTableExists(String tableName) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            DatabaseMetaData databaseMetadata = connection.getMetaData();

            ResultSet resultSet = databaseMetadata.getTables(
                    null, null, tableName.toUpperCase(), new String[]{"TABLE"});

            return resultSet.next();
        }
    }

    /**
     * Инициализация таблицы студентов.
     * @throws SQLException Если была ошибка во время SQL операций
     */
    private void initializeTables() throws SQLException {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            String sql =
                    "CREATE TABLE " + table + "(" +
                    "id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1)," +
                    "name VARCHAR(255), " +
                    "age INTEGER, " +
                    "arriveDate DATE, " +
                    "isDeducted BOOLEAN, " +
                    "someTimeField TIME" +
                    ")";
            statement.executeUpdate(sql);
        }
    }

    /**
     * Выбирает всех студентов (SELECT * FROM)
     * @return Коллекция студентов.
     * @throws SQLException Если была ошибка во время SQL операций
     */
    public Collection<Student> selectAll() throws SQLException {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM " + table);
            TreeSet<Student> students = new TreeSet<>();
            while (resultSet.next()) {
                Student student = new Student(
                        resultSet.getInt("id"),
                        resultSet.getString("name"),
                        resultSet.getInt("age"),
                        resultSet.getDate("arriveDate"),
                        resultSet.getBoolean("isDeducted"),
                        resultSet.getTime("someTimeField"));
                students.add(student);
            }
            return students;
        }
    }

    /**
     * Удаляет студента из базы.
     * @param student Объект студента.
     * @throws SQLException Если была ошибка во время SQL операций.
     */
    public void delete(Student student) throws SQLException {
        delete(student.getId());
    }

    /**
     * Удаляет студента из базы по его ID.
     * @param id ID студента.
     * @throws SQLException Если была ошибка во время SQL операций.
     */
    public void delete(int id) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("DELETE FROM " + table + " WHERE ID = " + id);
        }
    }

    public void update(Student student) throws SQLException {
        String sql =
                "UPDATE " + table + " SET " +
                        "name = ?," +
                        "age = ?," +
                        "arriveDate = ?," +
                        "isDeducted = ?," +
                        "someTimeField = ? WHERE id = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, student.getName());
            statement.setInt(2, student.getAge());
            Date arriveDate = new Date(student.getArriveDate().getTime());
            statement.setDate(3, arriveDate);
            statement.setBoolean(4, student.isDeducted());
            Time time = new Time(student.getSomeTimeField().getTime());
            statement.setTime(5, time);
            statement.setInt(6, student.getId());
            statement.execute();
        }
    }

    /**
     * Добавляет студента в базу. Возвращает новый объект студента с выданным ему ID.
     * @param student Вставляемый студент.
     * @return Вставляемый студент с обновленным ID.
     * @throws SQLException Если была ошибка во время SQL операций.
     */
    public Student insert(Student student) throws SQLException {
        String sql =
                "INSERT INTO " + table + "(name, age, arriveDate, isDeducted, someTimeField)" +
                "values(?, ?, ?, ?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, student.getName());
            statement.setInt(2, student.getAge());
            Date arriveDate = new Date(student.getArriveDate().getTime());
            statement.setDate(3, arriveDate);
            statement.setBoolean(4, student.isDeducted());
            Time time = new Time(student.getSomeTimeField().getTime());
            statement.setTime(5, time);
            statement.execute();

            ResultSet rs = statement.getGeneratedKeys();
            int nextId = -1;
            if (rs.next()) {
                // Замечательное и интуитивно понятное название для колонки!
                nextId = rs.getInt("1");
            }

            if (nextId == -1) {
                throw new SQLException("New id is not found.");
            }

            return new Student(nextId, student);
        }
    }

    /**
     * Очень опасный метод. Выполняет команду drop.
     * @param tablename Название сбрасываемой таблицы.
     * @throws SQLException Если была ошибка во время SQL операций.
     */
    public void dropTable(String tablename) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("DROP TABLE " + tablename);
        }
    }
}
