package ru.domain.peytob.model;

import java.util.Date;

/**
 * Описывает студента.
 */
public class Student implements Comparable<Student> {

    /**
     * ID студента в базе данных. Null, если он не зарегистрирован в базе.
     */
    private final int id;

    /**
     * Имя студента.
     */
    private String name;

    /**
     * Возраст.
     */
    private int age;

    /**
     * Дата поступления в университет (нужно было поле с датой)
     */
    private Date arriveDate;

    /**
     * Был ли студент отчислен (нужно было поле с bool)
     */
    private boolean isDeducted;

    /**
     * Просто поле со временем для условий задачи.
     */
    private Date someTimeField;

    public Student(int id, String name, int age, Date arriveDate, boolean isDeducted, Date someTimeField) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.arriveDate = arriveDate;
        this.isDeducted = isDeducted;
        this.someTimeField = someTimeField;
    }

    /**
     * Констуктор копирования с новым ID.
     * @param id Новое ID.
     * @param other Копируемый студент.
     */
    public Student(int id, Student other) {
        this(id, other.getName(), other.getAge(), other.getArriveDate(), other.isDeducted(), other.getSomeTimeField());
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getArriveDate() {
        return arriveDate;
    }

    public void setArriveDate(Date arriveDate) {
        this.arriveDate = arriveDate;
    }

    public boolean isDeducted() {
        return isDeducted;
    }

    public void setDeducted(boolean deducted) {
        isDeducted = deducted;
    }

    public Date getSomeTimeField() {
        return someTimeField;
    }

    public void setSomeTimeField(Date someTimeField) {
        this.someTimeField = someTimeField;
    }

    @Override
    public int compareTo(Student o) {
        return Integer.compare(o.getId(), id);
    }
}
