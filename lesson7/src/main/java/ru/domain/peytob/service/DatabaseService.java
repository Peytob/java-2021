package ru.domain.peytob.service;

import ru.domain.peytob.model.Student;
import ru.domain.peytob.repository.StudentRepository;

import java.sql.SQLException;
import java.util.Collection;

public class DatabaseService {
    private final StudentRepository repository;

    public DatabaseService(StudentRepository repository) {
        this.repository = repository;
    }

    public Collection<Student> selectAll() throws SQLException {
        return repository.selectAll();
    }

    public void delete(int id) throws SQLException {
        repository.delete(id);
    }

    public void delete(Student student) throws SQLException {
        delete(student.getId());
    }

    public void update(Student student) throws SQLException {
        repository.update(student);
    }

    public void insert(Student student) throws SQLException {
        repository.insert(student);
    }

    public void dropTable(String tablename) throws SQLException {
        repository.dropTable(tablename);
    }
}
