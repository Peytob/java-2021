package ru.domain.peytob.databaseProvider;

import org.apache.derby.jdbc.EmbeddedDataSource;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Драйвер для встроенной базы данных Derby
 */
public class DerbyProvider {
    /**
     * Фактически, пул подключений.
     */
    private EmbeddedDataSource dataSource;

    /**
     * Загруженные properties.
     */
    private final Map<String, String> properties = new HashMap<>();

    public DerbyProvider() throws IOException {
        loadProperties();
    }

    /**
     * Загрузка Properties из файла.
     * @throws IOException При проблемах при загрузке.
     */
    private void loadProperties() throws IOException {
        Properties properties = new Properties();
        try {
            properties.load(
                Thread.currentThread().getContextClassLoader().getResourceAsStream("derbyDatabase.properties"));
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                this.properties.put((String) entry.getKey(), (String) entry.getValue());
            }
        } catch (Exception e) {
            throw new IOException("Error occurred during loading properties");
        }
    }

    /**
     * Создает dataSource, загружает в него properties.
     * @return Созданный dataSource.
     */
    public EmbeddedDataSource getDataSource() {
        if (dataSource == null) {
            dataSource = new EmbeddedDataSource();
            dataSource.setDatabaseName(properties.get("database.name"));
            String username = properties.get("database.username");
            String password = properties.get("database.password");
            if (username != null && !username.isEmpty() && password != null && !password.isEmpty()) {
                dataSource.setUser(username);
                dataSource.setPassword(password);
            }
            System.setProperty("derby.language.sequence.preallocator", properties.get("derby.language.sequence.preallocator"));
            dataSource.setCreateDatabase("create");
        }

        return dataSource;
    }
}
