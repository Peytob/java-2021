package ru.domain.peytob.controller;

import org.junit.jupiter.api.Test;
import ru.domain.peytob.model.Case;
import ru.domain.peytob.model.Status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class FileServiceTest {
    @Test
    void test() throws IOException, ClassNotFoundException {
        ArrayList<Case> casesOriginal = new ArrayList<>();
        casesOriginal.add(new Case(5, "TEST1", "TEST2", "TEST3", "TEST4", Status.FAIL));
        casesOriginal.add(new Case(78, "2_TEST1", "2_TEST2", "2_TEST3", "2_TEST4", Status.COMPLETE));
        casesOriginal.add(new Case(46, "3_TEST1", "3_TEST2", "3_TEST3", "3_TEST4", Status.IN_WORK));

        FileService<ArrayList<Case>> alcFile = new FileService<>("ALC.ser");
        alcFile.write(casesOriginal);

        assertEquals(casesOriginal, alcFile.read());

        Case cs = new Case(46, "3_TEST1", "3_TEST2", "3_TEST3", "3_TEST4", Status.IN_WORK);
        FileService<Case> caseFile = new FileService<>("case.ser");
        caseFile.write(cs);
        assertEquals(cs, caseFile.read());
    }
}