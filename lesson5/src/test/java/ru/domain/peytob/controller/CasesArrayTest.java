package ru.domain.peytob.controller;

import ru.domain.peytob.model.Case;
import ru.domain.peytob.model.Status;

import static org.junit.jupiter.api.Assertions.*;

class CasesArrayTest {
    @org.junit.jupiter.api.Test
    void generateKey() {
        CasesArray array = new CasesArray();

        assertEquals(1, array.generateKey());

        array.addCase(new Case(16, "", "", "", "", Status.FAIL));
        assertEquals(17, array.generateKey());
    }
}