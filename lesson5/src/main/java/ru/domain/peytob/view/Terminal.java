package ru.domain.peytob.view;

import ru.domain.peytob.controller.CasesArray;
import ru.domain.peytob.controller.FileService;
import ru.domain.peytob.model.Case;
import ru.domain.peytob.model.Status;

import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;

public class Terminal {
    FileService<CasesArray> fileService;
    CasesArray cases;
    Scanner scanner;

    public Terminal() {
        scanner = new Scanner(System.in);

        System.out.print("Введите файл, хранящий в себе информацию с задачами (ничего для его отсутствия): ");
        String file = scanner.nextLine();

        if (!file.isBlank()) {
            fileService = new FileService<>(file + ".ser");
            try {
                cases = fileService.read();
            }

            catch (Exception exp) {
                System.out.println("Ошибка во время чтения файла: " + exp.getMessage());
            }
        }

        if (cases == null) {
            cases = new CasesArray(new HashMap<>());
        }
    }

    public String readString() {
        return scanner.nextLine().strip().toLowerCase(Locale.ROOT);
    }

    public Integer readCode() {
        int code = 0;
        boolean findCode = false;
        do {
            System.out.print("Введите код задачи (-1 для отмены): ");
            findCode = false;

            try {
                code = Integer.parseInt(scanner.nextLine());
            }

            catch (Exception ignored) {
                System.out.println("Код задачи не является числом!");
            }

            if (code == -1) {
                return null;
            }

            findCode = cases.checkCase(code);
        } while ( !findCode );

        return code;
    }

    public void run() {
        boolean running = true;

        while (running) {
            System.out.print("Введите комманду (help для помощи): ");
            String command = readString();

            /* TODO Если успею: переделать по типу:
               CommandProcessor - обработчик комманд
               ICommand - Интерфейс для команд

               В процессор:
               Map<command, ICommand> commands;
               commands[command].invoke(arguments)
            */

            if (command.isEmpty()) {
                System.out.println("Команда пуста");
            }

            else if (command.equals("help")) {
                showHelp();
            }

            else if (command.equals("create")) {
                System.out.println("Ввод новой задачи.");

                System.out.print("Введите название задачи: ");
                String title;
                do {
                    title = readString();
                } while (title.isEmpty());

                System.out.print("Введите описание задачи: ");
                String description;
                do {
                    description = readString();
                } while (description.isEmpty());

                System.out.print("Введите исходный код задачи (а тут лучше из файла читать): ");
                String source;
                do {
                    source = readString();
                } while (source.isEmpty());

                System.out.print("Введите имя исполнителя: ");
                String executor;
                do {
                    executor = readString();
                } while (executor.isEmpty());

                Integer key = cases.generateKey();
                Case newCase = new Case(key, source, title, description, executor);
                cases.addCase(newCase);

                System.out.println("Добавлена задача под кодом " + key);
            }

            else if (command.equals("delete")) {
                System.out.println("Удаление задачи.");

                Integer code = readCode();

                if (code != null) {
                    cases.delete(code);
                    System.out.println("Удалена задача под кодом " + code);
                }
            }

            else if (command.equals("showcase")) {
                Integer code = readCode();

                if (code != null) {
                    Case cs = cases.get(code);
                    System.out.println("Код задачи: " + cs.getCode());
                    System.out.println("Исполнитель: " + cs.getExecutor());
                    System.out.println("Описание: " + cs.getDescription());
                    System.out.println("Исполнитель: " + cs.getStatus());
                    System.out.println("Исходный код:");
                    System.out.println(cs.getSource());
                }
            }

            else if (command.equals("showlist")) {
                System.out.printf("%8s%30s%30s%10s\n", "Код", "Название", "Исполнитель", "Статус");
                for (Case item : cases.getCases()) {
                    System.out.printf("%8d%30s%30s%10s\n", item.getCode(), item.getTitle(), item.getExecutor(), item.getStatus().toString());
                }
            }

            else if (command.equals("save")) {
                if (fileService == null) {
                    System.out.print("Введите название файла: ");
                    fileService = new FileService<>(scanner.nextLine() + ".ser");
                }

                try {
                    fileService.write(cases);
                }

                catch (Exception exp) {
                    System.out.println("Ошибка: " + exp.getMessage());
                }
            }

            else if (command.equals("exit")) {
                return;
            }

            else if (command.equals("edit")) {
                System.out.println("Редактирование задачи.");
                Integer code = readCode();

                if (code != null) {
                    Case cs = cases.get(code);

                    int input = 0;

                    do {
                        System.out.println("1 - Изменить исполнителя задачи");
                        System.out.println("2 - Изменить статус задачи.");
                        System.out.println("3 - Изменить описание задачи.");
                        System.out.println("4 - Изменить заголовок задачи.");
                        System.out.println("5 - Изменить код задачи.");
                        System.out.println("0 - Выйти.");

                        System.out.print("Введите номер пункта меню: ");

                        try {
                            input = Integer.parseInt(scanner.nextLine());
                        }

                        catch (Exception exp) {
                            input = -1;
                        }

                        switch (input) {
                            case 1:
                                cs.setExecutor(scanner.nextLine());
                                break;

                            case 2:
                                System.out.println("1 - в процессе, 2 - завершена, 3 - неудача");
                                Integer inputStatus = 0;

                                try {
                                    inputStatus = Integer.parseInt(scanner.nextLine());
                                }

                                catch (Exception exp) {
                                    inputStatus = -1;
                                }

                                switch (inputStatus) {
                                    case 1:
                                        cs.setStatus(Status.IN_WORK);
                                        break;

                                    case 2:
                                        cs.setStatus(Status.COMPLETE);
                                        break;

                                    case 3:
                                        cs.setStatus(Status.FAIL);
                                        break;

                                    default:
                                        break;
                                }
                                break;

                            case 3:
                                cs.setDescription(scanner.nextLine());
                                break;

                            case 4:
                                cs.setTitle(scanner.nextLine());
                                break;

                            case 5:
                                cs.setSource(scanner.nextLine());
                                break;

                            case 0:
                                break;

                            default:
                                System.out.println("Неверный ввод");
                        }
                    } while (input != 0);
                }
            }
        }
    }

    public void showHelp() {
        System.out.println("Доступные команды:");
        System.out.println("help     - помощь");
        System.out.println("create   - добавить новое задание");
        System.out.println("delete   - удалить задачу:");
        System.out.println("edit     - редактировать задачу");
        System.out.println("showList - просмотреть список задач");
        System.out.println("showCase - подробную информацию о задаче");
        System.out.println("exit     - закрыть программу");
    }
}
