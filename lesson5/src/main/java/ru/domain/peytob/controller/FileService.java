package ru.domain.peytob.controller;

import java.io.*;

/**
 * Записывает сериализованные данные в файл и читает их обратно.
 * @param <T> Тип данных. Должен быть сереализуемым.
 */
public class FileService<T extends Serializable> {
    final String fileName;

    /**
     * Стандартный конструктор.
     * @param fileName Название файла.
     */
    public FileService(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Записывает все данные в файл.
     * @param data Записываемые данные.
     * @throws IOException Если что-то пошло не так с вводом-выводом в файл.
     */
    public void write(T data) throws IOException {
        try (FileOutputStream fstream = new FileOutputStream(fileName)) {
            ObjectOutputStream objstream = new ObjectOutputStream(fstream);
            objstream.writeObject(data);
        }
    }

    /**
     * Считывает все данные из файла.
     * @throws IOException Если что-то пошло не так с вводом-выводом в файл.
     * @throws ClassNotFoundException Если что-то не так с читываемыми данными.
     */
    public T read() throws IOException, ClassNotFoundException {
        T acc;
        try (FileInputStream fstream = new FileInputStream(fileName)) {
            ObjectInputStream objstream = new ObjectInputStream(fstream);

            acc = (T) objstream.readObject();
        }

        return acc;
    }
}
