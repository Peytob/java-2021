package ru.domain.peytob.controller;

import ru.domain.peytob.model.Case;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;

/**
 * Хранит в себе все задачи и обеспечивает интерфейс для работы с ними.
 * Сериализуется в файл для удобного ввода-вывода множества задач.
 */
public class CasesArray implements Serializable {
    @Serial
    private static final long serialVersionUID = 2L;

    HashMap<Integer, Case> cases;

    public CasesArray(HashMap<Integer, Case> cases) {
        this.cases = Objects.requireNonNullElseGet(cases, HashMap::new);
    }

    public CasesArray() {
        this(null);
    }

    public Collection<Case> getCases() {
        return cases.values();
    }

    /**
     * Проверяет наличие ключа Code в одной из задач.
     * @param code Ключ.
     * @return true, Если такой ключ уже занят.
     */
    public boolean checkCase(int code) {
        return cases.containsKey(code);
    }

    /**
     * Генерирует ключ для следующей задачи.
     * @return Новый ключ.
     */
    public Integer generateKey() {
        Integer maxKey = null;
        for (Integer i : cases.keySet()) {
            maxKey = (maxKey == null || i > maxKey) ? i : maxKey;
        }
        return (maxKey == null) ? 1 : maxKey + 1;
    }

    /**
     * Добавляет задачу.
     * @param newCase Новая задача.
     */
    public void addCase(Case newCase) {
        cases.put(newCase.getCode(), newCase);
    }

    /**
     * Удаляет задачу по ее коду.
     * @param code Код задачи.
     * @return True, если задача была найдена и удалена.
     */
    public boolean delete(int code) {
        Case t = null;
        if (cases.containsKey(code)) {
            t = cases.remove(code);
        }

        return t == null;
    }

    /**
     * Возвращает задачу по ее коду (или null, если такой нет)
     * @param code Код задачи
     * @return Задача (или null, если нет с таким кодом)
     */
    public Case get(Integer code) {
        return cases.get(code);
    }
}
