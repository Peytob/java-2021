package ru.domain.peytob.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.Objects;

/**
 * Информация о задаче.
 * Код задачи задается в виде произвольного целого числа.
 */
public class Case implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;

    protected Integer code;
    protected String source;
    protected String title;
    protected String description;
    protected String executor;
    protected Status status;

    public Case(Integer code, String source, String title, String description, String executor, Status status) {
        this.code = code;
        this.title = title;
        this.description = description;
        this.executor = executor;
        this.status = status;
        this.source = source;
    }

    public Case(Integer code, String source, String title, String description, String executor) {
        this(code, source, title, description, executor, Status.IN_WORK);
    }

    public Integer getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Case aCase = (Case) o;
        return Objects.equals(code, aCase.code) && Objects.equals(source, aCase.source) && Objects.equals(title, aCase.title) && Objects.equals(description, aCase.description) && Objects.equals(executor, aCase.executor) && status == aCase.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, source, title, description, executor, status);
    }
}
