package ru.domain.peytob.model;

/**
 * Текущий статус задачи.
 */
public enum Status {
    /**
     * Выполнение задачи в процессе.
     */
    IN_WORK,

    /**
     * Задача успешно выполнена.
     */
    COMPLETE,

    /**
     * Задачу выполнить не вышло.
     */
    FAIL
}
