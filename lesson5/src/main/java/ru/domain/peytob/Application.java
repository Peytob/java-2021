package ru.domain.peytob;

import ru.domain.peytob.view.Terminal;

public class Application {
    public static void main(String[] args) {
        Terminal terminal = new Terminal();
        terminal.run();
    }
}
