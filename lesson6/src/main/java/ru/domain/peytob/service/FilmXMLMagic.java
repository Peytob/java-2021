package ru.domain.peytob.service;

import ru.domain.peytob.model.serealization.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Класс, решающий поставленную задачу - перевод списка фильмов в послужные
 * списки их сценаристов и режиссеров.
 */
public class FilmXMLMagic {
    /**
     * Контекст для входных данных JAXB (FilmsCollectionStruct).
     */
    private final JAXBContext inputContext;

    /**
     * Сереализатор для выходных данных
     */
    private final Marshaller marshaller;

    /**
     * Контекст для входных данных JAXB (PeoplesStruct).
     */
    private final JAXBContext outputContext;

    /**
     * Десереализатор для входных данных.
     */
    private final Unmarshaller unmarshaller;

    public FilmXMLMagic() throws JAXBException {
        this.inputContext = JAXBContext.newInstance(FilmsCollectionStruct.class);
        this.unmarshaller = inputContext.createUnmarshaller();

        this.outputContext = JAXBContext.newInstance(PeoplesStruct.class);
        this.marshaller = outputContext.createMarshaller();
        this.marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    }

    private PersonStruct findPersonData(String name, FilmsCollectionStruct collection) {
        PersonStruct ps = new PersonStruct();
        ps.name = name;
        ps.films = new ArrayList<>();

        for (FilmStruct film : collection.collection) {
            PersonFilmStruct pfs = new PersonFilmStruct();
            pfs.title = film.title;
            pfs.functions = new LinkedList<>();

            for (ScreenwriterStruct screenwriter : film.screenwriters) {
                if (screenwriter.name.equals(name)) {
                    Function f = new Function();
                    f.name = "Сценарист";
                    pfs.functions.add(f);
                }
            }

            for (DirectorStruct director : film.directors) {
                if (director.name.equals(name)) {
                    Function f = new Function();
                    f.name = "Режиссер";
                    pfs.functions.add(f);
                }
            }

            if (pfs.functions.size() != 0) {
                ps.films.add(pfs);
            }
        }

        return ps;
    }

    public String convert(FilmsCollectionStruct collection) throws JAXBException {
        HashMap<String, PersonStruct> persons = new HashMap<>();

        for (FilmStruct film : collection.collection) {
            for (DirectorStruct person : film.directors) {
                if (!persons.containsKey(person.name)) {
                    persons.put(person.name, findPersonData(person.name, collection));
                }
            }

            for (ScreenwriterStruct person : film.screenwriters) {
                if (!persons.containsKey(person.name)) {
                    persons.put(person.name, findPersonData(person.name, collection));
                }
            }
        }

        PeoplesStruct po = new PeoplesStruct();
        po.persons = new LinkedList<>(persons.values());
        StringWriter writer = new StringWriter();
        marshaller.marshal(po, writer);
        return writer.toString();
    }

    public FilmsCollectionStruct read(File file) throws JAXBException {
        return (FilmsCollectionStruct) unmarshaller.unmarshal(file);
    }
}
