package ru.domain.peytob.model.serealization;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Небольшая структура для записи информации о человека во время сереализации.
 */
@XmlRootElement(name = "person")
public class PersonStruct {
    @XmlElement
    public String name;

    @XmlElement(name = "film")
    @XmlElementWrapper
    public Collection<PersonFilmStruct> films;
}
