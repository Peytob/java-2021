package ru.domain.peytob.model.serealization;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Класс, используемый во время сереализации. Оборачивает в себе коллекцию
 * фильмов, в которых учавствовал человек.
 */
@XmlRootElement(name = "film")
public class PersonFilmStruct {
    @XmlAttribute
    public String title;

    @XmlElement(name = "function")
    @XmlElementWrapper
    public Collection<Function> functions;
}
