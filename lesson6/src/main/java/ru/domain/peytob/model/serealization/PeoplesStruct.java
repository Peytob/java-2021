package ru.domain.peytob.model.serealization;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;

/**
 * Оболочка для записи информациях о режиссерах и сценаристах.
 */
@XmlRootElement(name = "people")
public class PeoplesStruct {
    @XmlElement(name = "person")
    public Collection<PersonStruct> persons;
}
