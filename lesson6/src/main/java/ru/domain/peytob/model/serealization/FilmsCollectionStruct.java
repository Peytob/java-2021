package ru.domain.peytob.model.serealization;

import ru.domain.peytob.model.Film;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Небольшой враппер для коллекций, использующийся во время десереализации данных.
 * В него превращаются входные данные из примера задачи, после чего коллекция в нем
 * может использоваться для перегона полученных FilmStruct в фильмы и их вывода.
 */
@XmlRootElement(name = "films")
@XmlAccessorType(XmlAccessType.FIELD)
public class FilmsCollectionStruct {
    /**
     * Коллекция для сереализации. Хранит в себе данные в подготовленном к выводу
     * формате.
     */
    @XmlElement(name = "film")
    public Collection<FilmStruct> collection;

    public FilmsCollectionStruct() {

    }

    public FilmsCollectionStruct(Collection<Film> collection) {
        this.collection = new ArrayList<>();
        for (Film film : collection) {
            this.collection.add((FilmStruct) film.getXmlStruct());
        }
    }
}
