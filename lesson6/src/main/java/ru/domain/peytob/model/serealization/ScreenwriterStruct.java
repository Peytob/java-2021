package ru.domain.peytob.model.serealization;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Очень маленький класс для сереализации данных. Позволяет
 * десереализовать строки вида <screenwriter name="Иванов"/> во
 * входных данных из примера.
 */
@XmlRootElement(name = "screenwriter")
public class ScreenwriterStruct {
    @XmlAttribute
    public String name = "";

    public ScreenwriterStruct(String name) {
        this.name = name;
    }

    public ScreenwriterStruct() {
        this("UNDEFINED");
    }
}
