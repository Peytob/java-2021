package ru.domain.peytob.model.serealization;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Небольшая структура для помощи во время десереализации данных о фильмах.
 * Позволяет корректно считывать данные вида <director name="Петров"/>
 */
@XmlRootElement(name = "director")
public class DirectorStruct {
    /**
     * Значение аттрибута name.
     */
    @XmlAttribute
    public String name = "";

    public DirectorStruct(String name) {
        this.name = name;
    }

    public DirectorStruct() {
        this("UNDEFINED");
    }
}
