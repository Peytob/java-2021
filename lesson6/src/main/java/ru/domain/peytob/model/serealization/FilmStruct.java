package ru.domain.peytob.model.serealization;

import ru.domain.peytob.model.Film;

import javax.xml.bind.annotation.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Небольшой враппер для фильмов, использующийся при десереализации данных.
 */
@XmlRootElement(name = "film")
@XmlAccessorType(XmlAccessType.FIELD)
public class FilmStruct {
    @XmlElement
    public String title;

    @XmlElement
    public String description;

    @XmlElement(name = "screenwriter")
    @XmlElementWrapper
    public List<ScreenwriterStruct> screenwriters;

    @XmlElement(name = "director")
    @XmlElementWrapper
    public List<DirectorStruct> directors;

    public FilmStruct() {
        this.title = "UNDEFINED";
        this.description = "UNDEFINED";
        this.screenwriters = new LinkedList<>();
        this.directors = new LinkedList<>();
    }

    public FilmStruct(Film film) {
        this.title = film.getTitle();
        this.description = film.getDescription();
        this.screenwriters = new LinkedList<>();

        for (String screenwriter : film.screenwriters) {
            this.screenwriters.add(new ScreenwriterStruct(screenwriter));
        }

        this.directors = new LinkedList<>();
        for (String director : film.directors) {
            this.directors.add(new DirectorStruct(director));
        }
    }
}
