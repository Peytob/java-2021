package ru.domain.peytob.model;

/**
 * Базовый класс для всех классов, у которых возможна немного нестандартная
 * сериализация в XML.
 */
public interface Serializable {
    public Object getXmlStruct();
}
