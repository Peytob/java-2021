package ru.domain.peytob.model;

import ru.domain.peytob.model.serealization.FilmStruct;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Данные о каком-либо фильме.
 */
public class Film implements Serializable {
    /**
     * Название фильма.
     */
    public String title;

    /**
     * Описание фильма.
     */
    public String description;

    /**
     * Сценаристы фильма.
     */
    public List<String> screenwriters;

    /**
     * Режиссеры фильма.
     */
    public List<String> directors;

    public Film(String title, String description, Collection<String> screenwriters, Collection<String> directors) {
        this.title = title;
        this.description = description;
        setScreenwriters(screenwriters);
        setDirectors(directors);
    }

    public Film() {
        this("DEFAULT", "DEFAULT", new LinkedList<>(), new LinkedList<>());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<String> getScreenwriters() {
        return screenwriters;
    }

    public void setScreenwriters(Collection<String> screenwriters) {
        this.screenwriters = new LinkedList<>(screenwriters);
    }

    public Collection<String> getDirectors() {
        return directors;
    }

    public void setDirectors(Collection<String> directors) {
        this.directors = new LinkedList<>(directors);
    }

    @Override
    public Object getXmlStruct() {
        return new FilmStruct(this);
    }
}
