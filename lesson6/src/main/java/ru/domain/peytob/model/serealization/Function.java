package ru.domain.peytob.model.serealization;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Класс, используемый для создания строк вида <function name="Сценарист"/>
 * в выводе.
 */
@XmlRootElement
public class Function {
    @XmlAttribute
    public String name;
}
