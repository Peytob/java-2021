package ru.domain.peytob.service;

import org.junit.jupiter.api.Test;
import ru.domain.peytob.model.serealization.FilmsCollectionStruct;

import javax.xml.bind.JAXBException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class FilmXMLMagicTest {
    @Test
    void demo() throws JAXBException, IOException {
        FilmXMLMagic magic = new FilmXMLMagic();
        FilmsCollectionStruct result = magic.read(new File("testInput.xml"));
        String xml = magic.convert(result);

        byte[] data = Files.readAllBytes(Paths.get("testOutput.xml"));
        String expected = new String(data);

        assertEquals(expected, xml);
    }
}
