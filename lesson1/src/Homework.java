import java.util.Scanner;

public class Homework
{
	/**
	 * Ф-я для округления числа до десятка.
	 * 1542 => 1540; 12638 => 12630; 14 => 10 ...
	 */
	public static long round10(short value)
	{
		return Math.round(value / 10L) * 10L;
	}

	/**
	 * Возвращает префикс для заданной степени двойки.
	 * 4 => B; 14 => KiB; 23 => GiB ...
	 * Максимальный прификс - Зетта, хотя в long не влезет больше нескольких эксабайт (~ 10^19 байт).
	 */
	public static String getPrefix(short degree)
	{
		if (degree < 10)
			return "B";

		if (degree < 20)
			return "KiB";

		if (degree < 30)
			return "MiB";

		if (degree < 40)
			return "GiB";

		if (degree < 50)
			return "TiB";

		if (degree < 60)
			return "PiB";

		if (degree < 70)
			return "EiB";

		if (degree < 80)
			return "ZiB";

		return "Unknown";
	}

	/**
	 * Перевод байтов в красивый и читаемый вид.
	 */
	public static String convert(long value)
	{
		// Получение степени двойки для данного значения
//		short degree2 = (short) (Math.log(value) / Math.log(2));
		short degree2 = (short) (63L - Long.numberOfLeadingZeros(value));

		// Только десятки полученной степени
		short roundDegree = (short) round10(degree2);

		float result = (float) value / (1L << roundDegree);

		return String.format("%.1f %s", result, getPrefix(degree2));
	}

	public static void main(String[] args)
	{
		System.out.println("Примеры из задания:");
		System.out.println("23 B => " + convert(23));
		System.out.println("1024 B => " + convert(1024));
		System.out.println("53692044905543 B => " + convert(53692044905543L));
		System.out.println("2^63 - 1 B => " + convert(9223372036854775807L));

		System.out.print("Введите ваше число байт: ");
		Scanner in = new Scanner(System.in);
		long val = in.nextLong();
		if (val > 0)
			System.out.println(val + " B => " + convert(val));
		else
			System.out.println("Введите положительное число :с");
	}
}
